<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
	<%@ page import="com.bus.ServletClass.*" %>
<%@ page import="javax.servlet.*"%>
<%
	ResultSet resultset = null;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<link rel="stylesheet"href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<head>
<link rel="shortcut icon" type="image/x-icon" href="bus.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Welcome</title>
</head>
<style>
body {
	
}

form {
	border: 3px solid #f1f1f1;
	max-width: 500px;
	margin: auto;
}

.input[type=text], input[type=text] {
	width: 50%;
	padding: 12px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 1px solid #ccc;
	box-sizing: border-box;
}

.passth {
	width: 35%;
	padding: 12px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 2px solid #ccc;
	box-sizing: border-box;
}
.btn {
	background-color: #f44336;
	border: none;
	color: white;
	padding: 10px 15px;
	font-size: 12px;
	cursor: pointer;
}
button {
	background-color: #4CAF50;
	color: white;
	padding: 14px 20px;
	margin: 8px 0;
	border: none;
	cursor: pointer;
	width: 50%;
}

button:hover {
	opacity: 0.8;
}

.cancelbtn {
	width: auto;
	padding: 10px 18px;
	background-color: #f44336;
}

.container {
	padding: 16px;
}

span.psw {
	float: right;
	padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
	span.psw {
		display: block;
		float: none;
	}
	.cancelbtn {
		width: 100%;
	}
}
</style>
<% HttpSession Session=request.getSession();  %>
<body style="margin: 200px">
	
	<%		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/BUS_DB", "root", "");

			Statement statement = connection.createStatement();

			resultset = statement.executeQuery("select distinct source from bus ");
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("select distinct destination from bus ");
	%>
	<form method="post" action="checkAvail">
		<center>
			<div class="container">
				<label><b>From</b></label> <input type="text"
					placeholder="Enter Source Point" id="ajax" name="source"
					list="json-datalist"   required>
				<datalist id="json-datalist"></datalist>
				<br>
				
				<script type="text/javascript">
	clearChildren = function (id) {
		var childArray = document.getElementById(id).children;
		if (childArray.length > 0) {
			console.log("clearing");
			document.getElementById(id).removeChild(childArray[0]);
			clearChildren(id);
		}
	}</script>
	<!-- <script type="text/javascript">		
		window.onload = function () {
			
	} -->
</script>
				<label><b> To </b></label> <input type="text"
					placeholder="Enter Destination point" id="ajaxx" name="dest"
					list="datalist" required>
				<datalist id="datalist"></datalist>
				<br>
				<script type="text/javascript">		
		window.onload = function () {
			var dataList = (document.getElementById('json-datalist'));
			<%while (resultset.next()) {	%>
			var val="<%=resultset.getString(1)%>";
			var varc = val.charAt(0).toUpperCase() + val.slice(1)
			var opt = document.createElement('option');
		      opt.value = varc;
		        dataList.appendChild(opt);
	  <%}%>
			
		
			var dataListt = document.getElementById('datalist');
			<%while (rs.next()) {%> 	
			var valt="<%= rs.getString(1) %>";
						var valct = valt.charAt(0).toUpperCase() + valt.slice(1)
						var optt = document.createElement('option');
						optt.value = valct;
						dataListt.appendChild(optt);
				<%}%>
	}
</script>
				<label><b>Date of Journey</b></label> <input type="date"
					class="passth" name="date" id=jdate placeholder="DD/MM/YYYY"
					required> <br>
				<script type="text/javascript">
					function checkDate() {
						var idate = document.getElementById("jdate"), dateReg = /(0[1-9]|[12][0-9]|3[01])[\/](0[1-9]|1[012])[\/]200[0-9]|20[1-9][0-9]/;
						console.log();
						if (!dateReg.test(idate.value)) {
							alert("Invalid Date!!! Enter a valid Date");
							document.getElementById('jdate').value = 0;
							return;
						}
						var dateString = document.getElementById('jdate').value;
						var myDate = new Date(dateString);
						var today = new Date();
						console.log(today);
						if (myDate > today || myDate.toDateString() == today.toDateString()) {
							document.getElementById('jdate').style.color = 'green';
						} else if (myDate - today == 0) {

						} else {
							alert("Invalid Date!!! Enter a valid Date");
							document.getElementById('jdate').value = 0;
						}
					}
				</script>
				<button type="submit" onclick="checkDate()">Search</button>
			</div>
	</form>
	<div class="container" style="background-color: #f1f1f1">
		
			<%if(Session.getAttribute("user")!=null){
				out.println("<form action='Logout' method='post'><button class='btn' name='button3' value='button3'style='width: auto;margin-top: 0px;margin-right: 0%;'><i class='fa fa-trash'></i> Logout</button></form>");
			}
			else{
				out.println("<form action='Login.jsp'><button type='submit' class='cancelbtn'>SIGN IN</button></form>");
			}%>
	</div>
	</center>

	<%
		//**Should I input the codes here?**
		} catch (Exception e) {
			out.println("wrong entry" + e);
		}
	%>
</body>
</html>