<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
		<%@ page import="com.bus.ServletClass.*" %>
		<%@ page import="java.io.*" %>
		<%@ page import="java.util.*" %>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" type="image/x-icon" href="bus.png" />
<title>Booking Bus</title>
</head>
<style>
table, th, td {
	border: 1px solid black;
}

select {
	width: 100px;
	margin: 10px;
}

select:focus {
	min-width: 100px;
	width: auto;
}
</style>
<% HttpSession sess = request.getSession(); 
if (sess.getAttribute("user") == null) {
	out.println("<script>alert('unAuthorized Accesss'); window.location.href = 'Login.jsp';</script>");
	Customer ccobj = null;
}else if(sess.getAttribute("customer")==null){
	out.println("<script>alert('Session Expired Due to Inactive!! Login again!'); window.location.href = 'Logout.jsp';</script>");
	//response.sendRedirect("Logout.jsp");
}
	Bus bbobj=(Bus)sess.getAttribute("bobj");
	boolean[] ar=bbobj.getVacantList();
	// for(int i=0;i<ar.length;i++){
	//	if(ar[i]==false)
	//		System.out.println(i+1);
	//} */
%>
<% 
	ArrayList<String> pic = (ArrayList<String>) bbobj.getPickup();
	ArrayList<String> drop=(ArrayList<String>)bbobj.getDrop();
%>
<body>
	<div class="container">

		<br>
		<lable> <b>Enter no.of Passenger to book: </b></lable>
		<select name="nost" id="nost" onChange="report(this.value)">
			<option>0</option>
			<option>1</option>
			<option>2</option>
			<option>3</option>
			<option>4</option>
			<option>5</option>
			<option>6</option>
		</select>
	</div>
	<br>
	<br>
	<b id="screens"> <script>
		var report = function(option) {

			 console.log(option); 
			var s = "";
			s += '<form method="post" action="BookTicket"><input type="hidden" name="nost" value="'+option+'">';
			for (var i = 1; i <= option; i++) {
				s=s+'<div><lable><b>Enter Passenger name: </b></lable>'+
				'<input type="text" id="pass'+i+'"name="pass'+i+'" placeholder="Name" required> <lable><b>Gender:'+
				'</b></lable><select name="gen'+i+'"><option> Male</option><option> Female</option></select><lable>'+
				'<b>Age: </b></lable><input type="number" id="age'+i+'" name="age'+i+'" placeholder="age" style="width:5%" min="4" max="100" required><lable>'+
				'<b>Seat: </b></lable><select name="seat'+i+'"><% for(int ii=0;ii<ar.length;ii++){if(ar[ii]==false) {%><option><%= (ii+1) %> </option> <%}} %></select>';
			if(i==option){
				s=s+"<br><lable>Choose Pickup Point</lable><select name='picks' style='width:8%;font-size: 10pt'><% for(String st: pic){%><option><%= st %></option><%}%></select>"+
				"<br><lable>Choose Drop Point</lable><select name='drops' style='width:8%;font-size: 10pt'><% for(String st: drop){%><option><%= st %></option><%}%></select><br>";
				s = s + '<button type="submit" style=" width:7%; height: 3%; background-color:#32CD32; font-size: 10pt;">Proceed</form>';
			}
			}
			
			/* console.log(s); */
			document.getElementById("screens").innerHTML = s;
		}
	</script>
	
</body>
</html>