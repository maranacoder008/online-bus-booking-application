<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="com.bus.ServletClass.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="shortcut icon" type="image/x-icon" href="bus.png" />
	<title>Adding a bus</title>
  </head>
<style>
body{
    padding: 20px 50px 0px 50px;
    width: auto;
    margin-top: 25px;
    box-sizing: border-box;
}

.wrapper{
    width: 80%;
    margin:auto;
}

.registration_form{
    width: 50%;
    margin: auto;
}

td{
    padding: 2px 5px 2px 5px;
}

.password{
    padding-top: 20px;
}
</style>
  <body>
    <div class="wrapper">
      <div class='registration_form'>
          <h2 style="margin:auto; width: 50%;">Bus Registration</h2>
          <br>
          <br><!-- <form action="addBus" method="post"> -->
        <table>
          <tr>
            <td>
                Bus Name
            </td>
            <td>
                Contact number
            </td>
          </tr>
          <tr>
              <td>
                  <input type='text' id="name" name="name" maxlength="16" required></td>
              <td>
                  <input type="text" id="phone" name="phone" minlength="10" maxlength="10" required></td>
            </tr>
            <tr>
              <td>
                Number of seats
              </td>
              <td>
                  Specification
              </td>
            </tr>
            <tr>
                <td>
                    <input type='number' id="noseat" name="noseat" min="1" max="100" required>
                </td>
                <td>
                    <select name="spec">
                        <option>AC Sleeper</option>
                        <option>AC Sleeper Seater</option>
                        <option>AC Seater</option>
                        <option>Non-AC Sleeper</option>
                        <option>Non-AC Sleeper Seater</option>
                        <option>Non-AC Seater</option>
                      </select>
                  </td>
              </tr>
              <tr><td>Source City</td><td>Destination City</td></tr>
              <tr>
                  <td>
                      <input type='text' id="source" name="source" maxlength="20" required>
                  </td>
                  <td><input type="text" id="dest" name="dest" maxlength="20" required></td>
              </tr>
              <tr><td>Pickup Time</td><td>Drop Time</td></tr>
              <tr>
                  <td>
                      <input type="time" id="ptime" name="ptime" required>
                  </td>
                  <td><input type="time" id="dtime" name="dtime" required></td>
              </tr>
              <tr class='password'><td>Password</td><td><input type="password" id="passw" name="passw" minlength="6" maxlength="15" required></td></tr>
              <tr class="password"><td>No.of Pickup Points</td><td><select name="nopk" id="nopk" onChange="Pickuppts(this.value)"><option>0</option><option>1</option><option>2</option>
              <option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option></select>
              </td></tr> 
              <tr class="password"><td>No.of Destination Points</td><td><select name="node" id="node" onChange="Destpts(this.value)"><option>0</option><option>1</option><option>2</option>
              <option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option></select>
              </td> </tr>
              <tr><td></td> <td><button type="button" onclick="seatdet()"> submit</button></td>
              </tr>
              <script>
              function Pickuppts(ss){
            	 
            	  console.log(ss);
            	  var s = "";
            	  for (var i = 1; i <= ss; i++) {
      				s=s+'<br><lable><b>Enter Source Point: </b></lable>'+
      				'<input type="text" id="sour'+i+'"name="sour'+i+'" placeholder="Enter Source" required> <lable><b>Time:'+
      				'</b></lable><input type="time" id="st'+i+'" name="st'+i+'" placeholder="time" required><br>';
      				
            	  }
            	  document.getElementById("screens").innerHTML = s;
              }
              function Destpts(ss){
            	  console.log(ss);
            	  var s = "";
            	  for (var i = 1; i <= ss; i++) {
        				s=s+'<br><lable><b>Enter Destination Point: </b></lable>'+
        				'<input type="text" id="destpt'+i+'"name="destpt'+i+'" placeholder="Enter Destination" required> <lable><b>Time:'+
        				'</b></lable><input type="time" id="dt'+i+'" name="dt'+i+'" placeholder="time" required><br>';
        				
              	  }
            	  document.getElementById("screes").innerHTML = s;
              }
              
              function seatdet(){
            	  	
            	  var no= document.getElementById("noseat").value;
            	  console.log("called"+no);
            	  if(no>1){
            	  var s = "";
            	  for (var i = 1; i <= no; i++) {
        				s=s+'<lable><b>Seat Type : </b></lable><select name="stype'+i+'"><option>Seater</option></select><lable><b> Enter Position : </b></lable>'+
        				'<input type="text" id="spo'+i+'"name="spo'+i+'" placeholder="Position" minlength="2" maxlength="8" required> <lable><b> Fare:'+
        				'</b></lable><input type="text" id="sfare'+i+'" name="sfare'+i+'" placeholder="Price" maxlength="5" required><br>';
              	  }
            	  s=s+'<center><br><button type="button" style=" width:17%; height: 3%; background-color:#32CD32; font-size: 10pt;" onclick="divb()">Proceed</button></center>';
            	  
            	  document.getElementById("seatas").innerHTML = s;
              }}
             
              function divb(){
            	 /*  console.log("came here"); */
            	  alert("Page Under Working!!"); 
            	  window.location.href = "opLogin.jsp";
            	  }
              </script>  
                  
         
        </table><br><b id="screens"></b><br><b id="screes"></b><br>
       <b id="seatas"></b>
        
      </div>
    </div><center>
    <!-- </form> --></center>
	
  </body>
</html>
<!--
name 
source
destination
specification
seat number
phone 
password
pickup time
drop time

seat number
seat type
position
fare

number of pickup points
number of drop points
-->