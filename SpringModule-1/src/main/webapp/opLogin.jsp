<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.bus.ServletClass.*" %>
<%@ page import="javax.servlet.http.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" type="image/x-icon" href="bus.png" />
<title>Operator</title>
</head>
<style>


form {
	border: 3px solid #f1f1f1;
	max-width: 500px;
	margin: auto;
	background-color: white;
}

.input[type=password], input[type=text] {
	width: 50%;
	padding: 12px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 1px solid #ccc;
	box-sizing: border-box;
}

.isns {
	width: 50%;
	padding: 12px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 1px solid #ccc;
	box-sizing: border-box;
}

.passth {
	width: 35%;
	padding: 12px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 2px solid #ccc;
	box-sizing: border-box;
}

button {
	background-color: #4CAF50;
	color: white;
	padding: 14px 20px;
	margin: 8px 0;
	border: none;
	cursor: pointer;
	width: 50%;
}

button:hover {
	opacity: 0.8;
}

.cancelbtn {
	width: auto;
	padding: 10px 18px;
	background-color: #f44336;
}

.container {
	padding: 16px;
}

.containerr {
	padding: 16px;
	width: 475px;
}

span.psw {
	float: right;
	padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
	span.psw {
		display: block;
		float: none;
	}
	.cancelbtn {
		width: 100%;
	}
}
</style>
<%
	HttpSession sess=request.getSession();
	if(sess.getAttribute("buser")!=null){
		response.sendRedirect("UserHome.jsp");
	}
	
%>
<body style=" margin: 200px; background-color:#999;">
	<center>
		<h2>Bus Opeartor Console</h2>
	</center>
	<form method="post" action="OpLog">
		<center>
			<div class="container">
				<label><b>Phone.No:</b></label> <input type="text"
					placeholder="Registered mobile no" id="mobno" name="mobno" minlength="10" maxlength="10" required>
				<br> <label><b> Password: </b></label> <input type="password"
					class="isns" placeholder="Enter password" id="passw" name="passw"
					required> <br>
					<button type="submit" onclick="checkDate()">Get Details</button>
			</div>
		</center>
	 </form>

	<center>
		<div class="containerr" style="background-color: #f1f1f1">
			 <button type="button" class="cancelbtn"
				onClick="parent.location='addBus.jsp'">Add a Bus</button> 
			<button type="button" class="cancelbtn" onClick="parent.location='Login.jsp'">Cancel</button>
		</div>
	</center>

</body>
</html>