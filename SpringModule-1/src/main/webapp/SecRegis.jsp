<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    	<%@ page import="com.bus.ServletClass.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" type="image/x-icon" href="bus.png" />
<title>Register</title>
</head>
<style>
body {
	
}

form {
	border: 3px solid #f1f1f1;
	max-width: 500px;
	margin: auto;
}

.input[type=password], input[type=text] {
	width: 50%;
	padding: 12px 20px;
	margin: 8px ;
	display: inline-block;
	border: 1px solid #ccc;
	box-sizing: border-box;
}

.isns {
	width: 50%;
	padding: 12px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 1px solid #ccc;
	box-sizing: border-box;
}

.passth {
	width: 35%;
	padding: 12px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 2px solid #ccc;
	box-sizing: border-box;
}

button {
	background-color: #4CAF50;
	color: white;
	padding: 14px 20px;
	margin: 8px 0;
	border: none;
	cursor: pointer;
	width: 50%;
}

button:hover {
	opacity: 0.8;
}

.cancelbtn {
	width: auto;
	padding: 10px 18px;
	background-color: #f44336;
}

.container {
	padding: 16px;
}

.containerr {
	padding: 16px;
	width: 475px;
}

span.psw {
	float: right;
	padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
	span.psw {
		display: block;
		float: none;
	}
	.cancelbtn {
		width: 100%;
	}
}
</style>


<body style="margin: 200px">
	
	<center>
		<h2>Welcome to Bus Booking System</h2>
	</center>
	<form method="post" action="SecAddAcc">
		<center>
			<div class="container">
            <label><b>Name:</b></label> <input style="margin-left:40px;" type="text"
					placeholder="Enter Name" id="name" name="name" required><br>
                    <label><b>Phnone.No:</b></label> <input type="text"
					placeholder="Enter Mobile No" id="phno" name="phno" pattern="^\d{10}$" required><br>
				<label><b>E-Mail:</b></label> <input type="text"
					placeholder="Enter E-Mail" style="margin-left:40px;" id="uname" name="uname" minlength="12" required>
				<br> <label><b> Password: </b></label> <input type="password"	
					class="isns" placeholder="Enter password" style="margin-left:18px;" id="passw" name="passw" minlength="7" maxlength="14"
					required> <br>
				<button type="submit">Register my Account!</button>
			</div>
		</center>
	</form>

	<center>
		<div class="containerr" style="background-color: #f1f1f1">
			<button type="button" class="cancelbtn"
				onClick="parent.location='SecLog.jsp'">Cancel</button>
		</div>
	</center>

</body>
</html>