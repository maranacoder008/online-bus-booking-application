<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.bus.ServletClass.*" %>
<%@ page import="javax.servlet.http.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<link rel="stylesheet"href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<head>
<link rel="shortcut icon" type="image/x-icon" href="bus.png" />
<title>Bill</title>
</head>
<body>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Ticket</title>

</head>
<style>
table, th, td {
	border: 1px solid black;
}

.button {
	line-height: 20px;
	width: 150px;
	font-size: 12pt;
	font-family: tahoma;
	margin-top: 700px;
	margin-right: 45%;
	background-color: #32CD32;
	position: absolute;
	top: 0;
	right: 0;
}
.button1 {
	line-height: 20px;
	width: 150px;
	font-size: 12pt;
	font-family: tahoma;
	margin-top: 700px;
	margin-right: 55%;
	background-color: #FA784B;
	position: absolute;
	top: 0;
	right: 0;
}
.button2 {
	line-height: 20px;
	width: 150px;
	font-size: 12pt;
	font-family: tahoma;
	margin-top: 700px;
	margin-right: 35%;
	background-color: #FA784B;
	position: absolute;
	top: 0;
	right: 0;
}
.btn {
	background-color: DodgerBlue;
	border: none;
	color: white;
	padding: 10px 15px;
	font-size: 16px;
	cursor: pointer;
	margin-top: 10px;
	margin-right: 5%;
	position:absolute;
	top:0;
	right:0;
}
</style>
<%
	HttpSession sess = request.getSession();
	if (sess.getAttribute("user") == null) {
		out.println("<html><script>alert('unAuthorized Access'); window.location.href = 'Login.jsp';</script></html>");
	}
	if (sess.getAttribute("ticObj") == null || sess.getAttribute("bobj") == null) {
		response.sendRedirect("welcome.jsp");
	}try{
	TicketDetails obj = (TicketDetails) sess.getAttribute("ticObj");
	Bus bobj = (Bus) sess.getAttribute("bobj");
	int no = obj.getno();
	String[] passname = obj.getpassname();
	int[] age = obj.getAges();
	boolean[] gen = obj.getGender();
	int[] seatno = obj.getSeatno();
	if(obj.checkDuplication(seatno)==false){
		System.out.println("came here "+seatno[0]);
		out.println("<html><script>alert('Provided seat numbers has Duplication!!! Kindly avoid choosing same seat numbers.'); window.location.href = 'ShowBus';</script></html>");
	}
	int[] fare = obj.getfare(bobj.getBusid());
	float gst = obj.getTotal() / 10;
	float pay = obj.getTotal() + gst;
	obj.setPay(pay);
	sess.setAttribute("payment", false);
	
%>
<body ><center><br><br><div style="background-color:powderblue; width:38%">
	<h2>
	<center><br>Conform Your Ticket</center>
</h2>
		<h3 style="margin-right: 52%">
			<br>Bus Name:
			<style="color:#32CD32">
<%=bobj.getName()%>
</style>
		</h3><form action='Logout' method='post'><button class='btn' name='button3' value='button3'style='width: auto;'><i class='fa fa-trash'></i> Logout</button></form>
		
		<h3 style="margin-right: 50%">
			Date of Journey:
			<%=bobj.getDate()%></h3>
		<br> <br>
		<h3>The Passenger List:</h3>
		<br>
		<table style='background-color: green'>
			<tr style='background-color: white'>
				<td style="width: 10%; background-color:powderblue;"><center>No</center></td>
				<td style="width: 20%; background-color:powderblue;"><center>Passenger Name</center></td>
				<td style="width: 10%; background-color:powderblue;"><center>Age</center></td>
				<td style="width: 16%; background-color:powderblue;"><center>Gender</center></td>
				<td style="width: 10%; background-color:powderblue;"><center>Seat.NO</center></td>
				<td style="width: 12%; background-color:powderblue;"><center>Fare</center></td>
			</tr>
			<%
				for (int i = 0; i < no; i++) {
			%>
			<tr style='background-color: white'>
				<td style="width: 10%; background-color:powderblue;"><center><%=(i + 1)%></center></td>
				<td style="width: 20%; background-color:powderblue;"><center><%=passname[i]%></center></td>
				<td style="width: 10%; background-color:powderblue;"><center><%=age[i]%></center></td>
				<%
					if (gen[i] == true) {
				%>
				<td style="width: 16%; background-color:powderblue;"><center>Male</center></td>
				<%
					} else {
				%>
				<td style="width: 16%; background-color:powderblue;"><center>Female</center></td>
				<%
					}
				%>
				<td style="width: 10%; background-color:powderblue;"><center><%=seatno[i]%></td>
				<td style="width: 12%; background-color:powderblue;"><center><%=fare[i]%></td>
			</tr>
			<%
				}
			%>

		</table>

		<h4 style="margin-right: 60%">
			Total Fare:<%=obj.getTotal()%></h4>
		<h5 style="margin-right: 60%">
			GST(10%):<%=gst%></h5>
		<h4 style="margin-right: 60%; color: #32CD32">
			Amount to Pay:<%=pay%></h4><br><br></center></div><center>
<%}catch(Exception e){
	e.printStackTrace();
	out.println("<html><script>alert('unAuthorized Access'); window.location.href = 'welcome.jsp';</script></html>");
} %>
		<form action="payment.jsp" method="post">
			<button class="button" type="submit" name="proceed"
				placeholder="Proceed to Pay"><b>Proceed to Pay</b>
		</form>
		
		<form action="welcome.jsp" method="post">
		<button type="submit" class="button1"><b>Cancel</b>
	</button></form>
	<button type="button" class="button2" onClick="history.back()"><b>Back</b>
	</button></center>
</body>
</html>
