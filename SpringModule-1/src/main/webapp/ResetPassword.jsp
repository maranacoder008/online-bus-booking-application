<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
    <%@ page import="com.bus.ServletClass.*" %>
<%@ page import="java.io.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" type="image/x-icon" href="bus.png" />
<title>Change Password</title>
</head>
<%
		try {
			HttpSession Session = request.getSession();

			if (Session.getAttribute("user") == null) {
				out.println("<script>alert('unAuthorized Accesss'); window.location.href = 'Login.jsp';</script>");
				Customer ccobj = null;
			}else if(Session.getAttribute("customer")==null){
				out.println("<script>alert('Session Expired Due to Inactive!! Login again!'); window.location.href = 'Logout.jsp';</script>");
				//response.sendRedirect("Logout.jsp");
			} 
		} catch (Exception e) {
			out.println("<html><script>alert('unAuthorized Accesss'); window.location.href = 'Login.jsp';</script></html>");
			RequestDispatcher rd = request.getRequestDispatcher("Login.jsp");
			rd.forward(request, response);
		}
	%>
<style>
body {
	
}

form {
	border: 3px solid #f1f1f1;
	max-width: 500px;
	margin: auto;
}

.input[type=password], input[type=text] {
	width: 50%;
	padding: 12px 20px;
	margin: 8px ;
	display: inline-block;
	border: 1px solid #ccc;
	box-sizing: border-box;
}

.isns {
	width: 50%;
	padding: 12px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 1px solid #ccc;
	box-sizing: border-box;
}

.passth {
	width: 35%;
	padding: 12px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 2px solid #ccc;
	box-sizing: border-box;
}

button {
	background-color: #4CAF50;
	color: white;
	padding: 14px 20px;
	margin: 8px 0;
	border: none;
	cursor: pointer;
	width: 50%;
}

button:hover {
	opacity: 0.8;
}

.cancelbtn {
	width: auto;
	padding: 10px 18px;
	background-color: #f44336;
}

.container {
	padding: 16px;
}

.containerr {
	padding: 16px;
	width: 475px;
}

span.psw {
	float: right;
	padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
	span.psw {
		display: block;
		float: none;
	}
	.cancelbtn {
		width: 100%;
	}
}
</style>
<body style="margin: 200px">
	
	<center>
		<h2>Change Password:</h2>
	</center>
	<form method="post" action="ChangePass">
		<center>
			<div class="container">
            <br> <label><b> Old Password: </b></label> <input type="password"	
					class="isns" placeholder="Enter password" style="margin-left:18px;" id="passw1" name="passw1"  maxlength="14"
					required> <br>
				<br> <label><b> New Password: </b></label> <input type="password"	
					class="isns" placeholder="Enter password" style="margin-left:18px;" id="passw2" name="passw2" minlength="7" maxlength="14"
					required> <br>
				<br> <label><b> Confirm Password: </b></label> <input type="password"	
					class="isns" placeholder="Enter password" style="margin-left:18px;" id="passw3" name="passw3" minlength="7" maxlength="14"
					required> <br>
				<button type="submit">Change Password</button>
			</div>
		</center>
	</form>

	<center>
		<div class="containerr" style="background-color: #f1f1f1">
			<button type="button" class="cancelbtn"
				onClick="parent.location='UserHome.jsp'">Cancel</button>
		</div>
	</center>
</body>
</html>