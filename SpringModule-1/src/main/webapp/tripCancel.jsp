<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="com.bus.ServletClass.*" %>
    <%@ page import="javax.servlet.http.*" %>
    <%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Trip Sheet</title>
<link rel="shortcut icon" type="image/x-icon" href="bus.png" />
</head>
<style>
<style>
table, th, td {
	border: 1px solid black;
}

.button {
	line-height: 20px;
	width: 150px;
	font-size: 12pt;
	font-family: tahoma;
	margin-top: 750px;
	margin-right: 45%;
	background-color: #32CD32;
	position: absolute;
	top: 0;
	right: 0;
}
.button1 {
	line-height: 20px;
	width: 150px;
	font-size: 12pt;
	font-family: tahoma;
	margin-top: 750px;
	margin-right: 75%;
	background-color: #FA784B;
	position: absolute;
	top: 0;
	right: 0;
}
.button2 {
	line-height: 20px;
	width: 150px;
	font-size: 12pt;
	font-family: tahoma;
	margin-top: 750px;
	margin-right: 15%;
	background-color: #FA784B;
	position: absolute;
	top: 0;
	right: 0;
}
.container{
    width: 400px;
    margin: 0 auto;
}

a.print{
   margin-right: -;
    border-right-width: 2px;
    padding-right: 20px;
    padding-top: 5px;
    padding-left: 20px;
    padding-bottom: 5px;
    height: 27px;
    color: #32CD32;
    margin-left: 25%;
    margin-top: 4%;
}
i.fa.fa-print{
    margin-right: 5px;
}
a.print:hover{
    background: linear-gradient(#DC143C, #e3647e)
}
.btn {
	background-color: DodgerBlue;
	border: none;
	color: white;
	padding: 10px 15px;
	font-size: 16px;
	cursor: pointer;
	margin-top: 10px;
	margin-right: 5%;
	position:absolute;
	top:0;
	right:0;
}
@media print {
    tr.vendorListHeading {
        background-color: #1a4567 !important;
        -webkit-print-color-adjust: exact; 
    }
}

@media print {
    .vendorListHeading th {
        color: white !important;
    }
}
</style>
<body><form action='Logout' method='post'><button class='btn' name='button3' value='button3'style='width: auto;'><i class='fa fa-trash'></i> Logout</button></form>
		<div id='printMe'><center>
<% 
	HttpSession sess=request.getSession();
	try{
	if(sess.getAttribute("opobj")==null  ){
		out.println("<html><script>alert('unAuthorized Access'); window.location.href = 'opLogin.jsp';</script></html>");
	}
	
	Operator obj=(Operator)sess.getAttribute("opobj");
	
	ArrayList<Integer> seatno=obj.getSeat();
	ArrayList<String> passname=obj.getPassname();
	ArrayList<Boolean> gender=obj.getGender();
	ArrayList<Integer> age=obj.getAge();
	ArrayList<String> pkpoint=obj.getPickpt();
	ArrayList<String> droppoint=obj.getDroppt();
	ArrayList<Integer> fare=obj.getFare();
	ArrayList<String> phone=obj.getPhone();
	//System.out.println(phone);
	float gst=obj.getTotal()/10;
	float gt=obj.getTotal()+gst;
%>
<!-- <script>
function myFunction() {
    window.print();
}
</script> -->
		<br>
		<h2><center> Passenger List</center></h2>
		
		<h3 style="margin-right: 20%">
			Date of Trip:
				<%=sess.getAttribute("date")%></h3>
		<br>
		<h3 >The Passenger List:</h3>
		<br><center>
		<table style='background-color: green'>
			<tr style='background-color: #ffc266'>
				<td style="width: 5%"><center>Seat No</center></td>
				<td style="width: 15%"><center>Passenger Name</center></td>
				<td style="width: 6%"><center>Gender</center></td>
				<td style="width: 5%"><center>Age</center></td>
				<td style="width: 15%"><center>Pickup Point</center></td>
				<td style="width: 15%"><center>Drop Point</center></td>
				<td style="width: 15%"><center>Mobile No</center></td>
				<td style="width: 10%"><center>Fare</center></td>
			</tr>
			<%
				for (int i = 0; i < seatno.size(); i++) {
			%>
			<tr style='background-color: white'>
				<td style="width: 10%"><center><%=seatno.get(i)%></center></td>
				<td style="width: 15%"><center><%=passname.get(i)%></center></td>
				
				<%
					if (gender.get(i) == true) {
				%>
				<td style="width: 6%"><center>Male</center></td>
				<%
					} else {
				%>
				<td style="width: 6%"><center>Female</center></td>
				<%
					}
				%>
				<td style="width: 5%"><center><%=age.get(i)%></center></td>
				<td style="width: 15%"><center><%=pkpoint.get(i)%></td>
				<td style="width: 15%"><center><%=droppoint.get(i)%></td>
				<td style="width: 15%"><center><%=phone.get(i)%></td>
				<td style="width: 10%"><center><%=fare.get(i)%></td>
			</tr>	
			<%
				}
			%>

		</table></center>

		<h4 style="margin-right: 30%">
			Total Collection:<%=obj.getTotal()%></h4>
		<h5 style="margin-right: 30%">
			Collected GST(10%):<%=gst%></h5>
		<h4 style="margin-right: 28%">
			Grant Total:<%=gt%></h4> 
<%}catch(Exception e){
	e.printStackTrace();
	out.println("<html><script>alert('You are not allowed to access this page'); window.location.href = 'OpHome.jsp';</script></html>");

} %>
		</div><center>
	
	
 <button type="button" class="button2" onClick="printDiv('printMe')"><i class="fa fa-print"></i><b>Print Sheet</b>
	</button> <br>
	<button type="submit" class="button1" onClick="window.location.href = 'OpHome.jsp';"><b> Back</b></button><br>
 <form action="CancelTrip" method="post"><button type="submit" class="button" onClick="alert('Refund Initialized!!');"><b>Cancel Trip</b></button></form>
	</center>
	<script>
		function printDiv(divName){
			
				console.log("here");
			var printContents = "<html><b>E-Bill</b></html>"+document.getElementById(divName).innerHTML;
			var originalContents = document.body.innerHTML;
			document.body.innerHTML = printContents;
			window.print();
			document.body.innerHTML = originalContents;
			
		}
	</script>
</body>
</html>