<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="com.bus.ServletClass.*" %>
    <%@ page import="javax.servlet.http.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" type="image/x-icon" href="bus.png" />
<title>Ticket</title>

</head>

<style>
table, th, td {
	border: 1px solid black;
}

.button {
	line-height: 20px;
	width: 150px;
	font-size: 12pt;
	font-family: tahoma;
	margin-top: 750px;
	margin-right: 45%;
	background-color: #32CD32;
	position: absolute;
	top: 0;
	right: 0;
}
.button1 {
	line-height: 20px;
	width: 150px;
	font-size: 12pt;
	font-family: tahoma;
	margin-top: 750px;
	margin-right: 55%;
	background-color: #FA784B;
	position: absolute;
	top: 0;
	right: 0;
}
.button2 {
	line-height: 20px;
	width: 150px;
	font-size: 12pt;
	font-family: tahoma;
	margin-top: 750px;
	margin-right: 35%;
	background-color: #FA784B;
	position: absolute;
	top: 0;
	right: 0;
}
.container{
    width: 400px;
    margin: 0 auto;
}

a.print{
   margin-right: -;
    border-right-width: 2px;
    padding-right: 20px;
    padding-top: 5px;
    padding-left: 20px;
    padding-bottom: 5px;
    height: 27px;
    color: #32CD32;
    margin-left: 25%;
    margin-top: 4%;
}
i.fa.fa-print{
    margin-right: 5px;
}
a.print:hover{
    background: linear-gradient(#DC143C, #e3647e)
}
.btn {
	background-color: DodgerBlue;
	border: none;
	color: white;
	padding: 10px 15px;
	font-size: 16px;
	cursor: pointer;
	margin-top: 10px;
	margin-right: 5%;
	position:absolute;
	top:0;
	right:0;
}
</style>
<body><form action='Logout' method='post'><button class='btn' name='button3' value='button3'style='width: auto;'><i class='fa fa-trash'></i> Logout</button></form>
		<div id='printMe'><center>
<% 
	HttpSession sess=request.getSession();
	try{
	if(sess.getAttribute("user")==null  ){
		out.println("<html><script>alert('unAuthorized Access'); window.location.href = 'Login.jsp';</script></html>");
	}
	if((Boolean)sess.getAttribute("payment")==false){
		out.println("<html><script>alert('You are Pending with payment!!'); window.location.href = 'payment.jsp';</script></html>");
	}
	Bus bobj=(Bus)sess.getAttribute("bobj");
		TicketDetails obj = (TicketDetails) sess.getAttribute("ticObj");
		int no = obj.getno();
		String[] passname = obj.getpassname();
		int[] age = obj.getAges();
		boolean[] gen = obj.getGender();
		int[] seatno = obj.getSeatno();
		int[] fare = obj.getfare();
		float gst = obj.getTotal() / 10;
		float pay = obj.getPay();

	
%>
<!-- <script>
function myFunction() {
    window.print();
}
</script> -->

		<h2><center> Travel Ticket</center></h2>
		<h3 style="margin-right: 5%">
			<center><br><br><br>Bus Name:<%=bobj.getName() %>
			</h3>
		<h4 style="margin-right: 5%">Oprator Number:
			<%= bobj.getPhone() %>
			</h4>
		<h3 style="margin-right: 5%">
			Date of Journey:
				<%=bobj.getDate()%></h3>
		<br> <br>
		<h4 style="margin-right: 18%">Pickup Point & Time: <%=obj.getPickup() %></h4>
		<h4 style="margin-right: 20%">Drop Point & Time: <%=obj.getDrop() %></h4>
		<h3 >The Passenger List:</h3>
		<br>
		<table style='background-color: green'>
			<tr style='background-color: #68FDDA'>
				<td style="width: 10%"><center>No</center></td>
				<td style="width: 20%"><center>Passenger Name</center></td>
				<td style="width: 10%"><center>Age</center></td>
				<td style="width: 16%"><center>Gender</center></td>
				<td style="width: 10%"><center>Seat.No</center></td>
				<td style="width: 12%"><center>Fare</center></td>
			</tr>
			<%
				for (int i = 0; i < no; i++) {
			%>
			<tr style='background-color: white'>
				<td style="width: 10%"><center><%=(i + 1)%></center></td>
				<td style="width: 20%"><center><%=passname[i]%></center></td>
				<td style="width: 10%"><center><%=age[i]%></center></td>
				<%
					if (gen[i] == true) {
				%>
				<td style="width: 16%"><center>Male</center></td>
				<%
					} else {
				%>
				<td style="width: 16%"><center>Female</center></td>
				<%
					}
				%>
				<td style="width: 10%"><center><%=seatno[i]%></td>
				<td style="width: 12%"><center><%=fare[i]%></td>
			</tr>	
			<%
				}
			%>

		</table>

		<h4 style="margin-right: 30%">
			Total Fare:<%=obj.getTotal()%></h4>
		<h5 style="margin-right: 30%">
			GST(10%):<%=gst%></h5>
		<h4 style="margin-right: 28%">
			Amount Paid:<%=pay%></h4> 
<%}catch(Exception e){
	if(sess.getAttribute("user")==null){
	out.println("<html><script>alert('unAuthorized Access'); window.location.href = 'Login.jsp';</script></html>");}
	else{
	out.println("<html><script>alert('You are not allowed to access this page'); window.location.href = 'UserHome.jsp';</script></html>");}

} %>
		<h4 style="margin-right: 17%">
			Payment Reference id: K987654321234567889AZ</h4></center>
		</div><center>
	
		<button type="submit" class="button1" onClick="window.location='UserHome.jsp'"><b>Home</b>
	</button>
 <button type="button" class="button2" onClick="printDiv('printMe')"><i class="fa fa-print"></i><b>Print Ticket</b>
	</button> 
<%-- 		<button type="button" class="button2"	onclick="printJS('<html><h3>Hello world this is a testing page...i will kiill uu</h3></html>','html')">Print Ticket</button>
 --%>	<button type="button" class="button" onClick="window.location='welcome.jsp'"><b>Book More</b></button>
	</center>
				
	<script>
		function printDiv(divName){
			
				
			var printContents = "E-Ticket"+document.getElementById(divName).innerHTML+"<html><br><b>Our responsibilities include:</b><br> "+
			"(1) Issuing a valid ticket (a ticket that will be accepted by "+
					"the bus operator) for its network of bus operators.<br>"+
					"(2) Providing refund and support in the event of "+
					"cancellation<br>"+
					"(3) Providing customer support and information in case of any delays / inconvenience.<br>"+
					"<b>Our responsibilities do not include:</b><br>"+
					"(1) The bus operator's bus not departing / reaching on time.<br>(2) The bus operator's employees being rude.<br>"+
					"(3) The bus operator's bus seats etc not being up to the customer's expectation.<br>(4) The bus operator canceling the trip due to unavoidable reasons.<br>"+
					"(5) The baggage of the customer getting lost / stolen / damaged.<br>(6) The bus operator changing a customer's seat at the last minute to accommodate a lady / child.<br>"+
					"(7) The customer waiting at the wrong boarding point (please call the bus operator to find out the exact boarding point if you are not a regular traveler on that particular bus).<br>"+
					"(8) The bus operator changing the boarding point and/or using a pick-up vehicle at the boarding point to take customers to the bus departure point";
			var originalContents = document.body.innerHTML;
			document.body.innerHTML = printContents;
			console.log(printContents);

			window.print();
			document.body.innerHTML = originalContents;
			
		}
	</script>
</body>
</html>