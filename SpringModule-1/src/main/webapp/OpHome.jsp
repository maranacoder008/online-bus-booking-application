<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="com.bus.ServletClass.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" type="image/x-icon" href="bus.png" />
<title>Home</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Add icon library -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></head>
<style>
.button {
	line-height: 12px;
	width: 18%;
	font-size: 8pt;
	font-family: tahoma;
	margin-top: 3px;
	margin-right: 2px;
	position: absolute;
	top: 0;
	right: 0;
}
.passth {
	width: 15%;
	padding: 12px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 2px solid #ccc;
	box-sizing: border-box;
}
.btn {
	background-color: DodgerBlue;
	border: none;
	width: 20%;
	color: white;
	padding: 12px 16px;
	font-size: 16px;
	cursor: pointer;
}
body { 
    background-image: url('');
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-position: center; 
    background-size: cover;
}

/* Darker background on mouse-over */
.btn:hover {
	background-color: RoyalBlue;
}
</style>

<body style=" margin: 200px;">
<%
		try {
			HttpSession Session = request.getSession();

			if (Session.getAttribute("opobj") == null) {
				out.println("<script>alert('unAuthorized Accesss'); window.location.href = 'opLogin.jsp';</script>");
				
			} 
		} catch (Exception e) {
			out.println("<html><script>alert('unAuthorized Accesss'); window.location.href = 'opLogin.jsp';</script></html>");
			RequestDispatcher rd = request.getRequestDispatcher("opLogin.jsp");
			rd.forward(request, response);
		}
	%>
	
	<center>
		<div><center><form action="OpAction" method="post">
			<label><b>Select Date to Fetch Data</b></label> <input type="date"
					class="passth" name="date" id=jdate placeholder="DD/MM/YYYY"
					required> <br>
				<script type="text/javascript">
					function checkDate() {
						var idate = document.getElementById("jdate"), dateReg = /(0[1-9]|[12][0-9]|3[01])[\/](0[1-9]|1[012])[\/]200[0-9]|20[1-9][0-9]/;
						console.log();
						if (!dateReg.test(idate.value)) {
							alert("Invalid Date!!! Enter a valid Date");
							document.getElementById('jdate').value = 0;
							return;
						}
						var dateString = document.getElementById('jdate').value;
						var myDate = new Date(dateString);
						var today = new Date();
						console.log(today);
						if (myDate > today) {
							document.getElementById('jdate').style.color = 'green';
						} else if (myDate - today == 0) {

						} else {
							alert("Invalid Date!!! Enter a valid Date");
							document.getElementById('jdate').value = 0;
						}
					}
				</script><br>
				
			
				<br>
				<button type="submit" class="btn" name="button1" value="button1"
					style="width: 30%;" onClick="checkDate()">
					<i class="fa fa-cloud"></i> Get Trip Sheet
				</button><br><br>
				<button type="submit" class="btn" name="button4" value="button4"
					style="width: 30%;" onClick="checkDate()">
					<i class="fa fa-bars"></i> Cancel Trip
				</button><br><br></form><form action="OpAction" method="post">
				<button class="btn" name="button2" value="button2"
					style="width: 30%;">
					<i class="fa fa-car" style="font-size:20px;color:red;"></i>  Delete Bus
				</button><br>
					<br>
				<button class="btn" name="button3" value="button3"
					style="width: 30%;">
					<i class="fa fa-trash"></i> Logout
				</button>
			</form></center>
		</div>
	</center>
</body>
</html>