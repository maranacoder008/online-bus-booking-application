<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
    <%@ page import="com.bus.ServletClass.*" %>
<%@ page import="java.io.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="shortcut icon" type="image/x-icon" href="bus.png" />
<title>Home</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.button {
	line-height: 12px;
	width: 18px;
	font-size: 8pt;
	font-family: tahoma;
	margin-top: 3px;
	margin-right: 2px;
	position: absolute;
	top: 0;
	right: 0;
}

.btn {
	background-color: DodgerBlue;
	border: none;
	color: white;
	padding: 12px 16px;
	font-size: 16px;
	cursor: pointer;
}
body { 
    background-image: url('');
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-position: center; 
    background-size: cover;
}

/* Darker background on mouse-over */
.btn:hover {
	background-color: RoyalBlue;
}
</style>
</head>
<script>
	var func = function() {
		console.log("Called Here");
	}
</script>

<body style="margin: 200px;">

	<%
		try {
			HttpSession Session = request.getSession();

			if (Session.getAttribute("user") == null) {
				out.println("<script>alert('unAuthorized Accesss'); window.location.href = 'Login.jsp';</script>");
				Customer ccobj = null;
			}else if(Session.getAttribute("customer")==null){
				out.println("<script>alert('Session Expired Due to Inactive!! Login again!'); window.location.href = 'Logout.jsp';</script>");
				//response.sendRedirect("Logout.jsp");
			} else {
				//System.out.println(Session.getAttribute("cobj"));
				Customer ccobj = (Customer) Session.getAttribute("cobj");
				String s = "Welcome Mr." + ccobj.getName();
				//System.out.println(Session.getAttribute("user"));
				out.println("<center><h3>" + s + "</h3></center>");
			}
		} catch (Exception e) {
			out.println("<html><script>alert('unAuthorized Accesss'); window.location.href = 'Login.jsp';</script></html>");
			RequestDispatcher rd = request.getRequestDispatcher("Login.jsp");
			rd.forward(request, response);
		}
	%>
	<center>
		<div>
			<form action="UserHome" method="post">
			<button type="submit" class="btn" name="button5" value="button5"
					style="width: 192px; background-color: #33FFCE;">
					<i class="fa fa-key"></i> Change Password
				</button>
				<button type="submit" class="btn" name="button4" value="button4"
					style="width: 182px; background-color: #f44336;">
					<i class="fa fa-bars"></i> Cancelled Tickets
				</button>
				<button type="submit" class="btn" name="button1" value="button1"
					style="width: 182px;">
					<i class="fa fa-bars"></i> History
				</button>
				<button class="btn" name="button2" value="button2"
					style="width: 182px;">
					<i class="fa fa-home"></i> Book A Ticket
				</button>

				<button class="btn" name="button3" value="button3"
					style="width: 182px;">
					<i class="fa fa-trash"></i> Logout
				</button>
			</form>
		</div>
	</center>
</body>
</html>