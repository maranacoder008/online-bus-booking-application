//$Id$
package com.bus.smeller.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller

public class DashBoardContoller {

	@GetMapping(value = "/welcome")
	public ModelAndView welcome(){
		ModelAndView model = new ModelAndView();
    	model.setViewName("welcome");
    	return model;
	}
}
