package com.bus.ServletClass;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.sql.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class checkAvail
 */
@WebServlet("/checkAvail")
public class checkAvail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public checkAvail() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		// System.out
		// .println(request.getParameter("date") +
		// request.getParameter("source") + request.getParameter("dest"));
		try {
			//System.out.println(history.back()");
			HttpSession Session = request.getSession();
			Session.setAttribute("source", request.getParameter("source"));
			Session.setAttribute("dest", request.getParameter("dest"));
			Session.setAttribute("date", request.getParameter("date"));
			String dat = request.getParameter("date");
			if(dat.charAt(2)=='/'){
				dat=dat.substring(6)+"-"+dat.substring(3,5)+"-"+dat.substring(0,2);
			}
			Class.forName("com.mysql.jdbc.Driver");

			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/BUS_DB", "root", "");
			PreparedStatement stmt = connection.prepareStatement("select * from bus where source=? AND destination=? order by pickuptime");
			stmt.setString(1, request.getParameter("source"));
			stmt.setString(2, request.getParameter("dest"));
			ResultSet resultset = stmt.executeQuery();
			ResultSet rs = resultset;
			int i=1;
			if (resultset.next() == true) {
				out.println(
						"<html><head><link rel='shortcut icon' type='image/x-icon' href='bus.png' /><title>Avail Bus</title></head><body><center><br><br><br> <div id='printMe' style='background-color:powderblue;padding: 16px;width: 60%;'> <table width='1000' border='1' cellpadding='1' cellspacing='1'> <h2> Available Buses:</h2> <tr><th>S.No</th><th>Name of the Travels</th><th>Date</th><th>Specification</th> <th>Departure</th><th>Destination</th> <th>Book Ticket</th> </tr>");

				do {
					// System.out.println(rs.getString(2));
					out.println("<form method='post' action='ShowBus'><input type='hidden' name='date' value=" + dat
							+ "><tr><td><center>" + (i++)+ "</td><td><center>" + rs.getString(2) + "</td><td><center>" + dat + "</td><td><center>"
							+ rs.getString(5) + "</td><td><center>" + rs.getString(9) + "</td><td><center>"
							+ rs.getString(10) + "</td><td><center><button type='submit' name='bus' value="
							+ rs.getInt(1) + " style='background-color:#32CD32'> Book Now </button></form></td><tr>");
				} while (rs.next());
				out.println("</table></div></center></body></html>");
			} else {
				out.println("<html><script>alert('No Services Available for this route')</script></html>");
				System.out.println("No Services Available for this route");
				RequestDispatcher rd = request.getRequestDispatcher("/welcome.jsp");
				rd.include(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
