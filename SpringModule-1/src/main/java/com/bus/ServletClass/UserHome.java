package com.bus.ServletClass;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class UserHome
 */
@WebServlet("/UserHome")
public class UserHome extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserHome() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		try {
			HttpSession Session = request.getSession();
			if (Session.getAttribute("user") == null) {
				out.println(
						"<html><script>alert('unAuthorized Access'); window.location.href = 'Login.jsp';</script></html>");
				// response.sendRedirect("Login.jsp");
			}
			else if(Session.getAttribute("customer")==null){
				out.print("<html><script>alert('Session Expired!! Try Again'); window.location.href = 'Logout.jsp';</script></html>");
			}
			else {
				out = response.getWriter();

				if (request.getParameter("button1") != null) {
					Customer cust = (Customer) Session.getAttribute("customer");
					System.out.println(cust);
					response.sendRedirect("showHistory.jsp");
				} else if (request.getParameter("button2") != null) {
					response.sendRedirect("welcome.jsp");
				} else if (request.getParameter("button3") != null) {
					RequestDispatcher rd = request.getRequestDispatcher("/Logout");
					rd.forward(request, response);
				}else if (request.getParameter("button4") != null) {
					Customer cust = (Customer) Session.getAttribute("customer");
					System.out.println(cust);
					out.print(cust.getCancelHistory((int) Session.getAttribute("user")));
				}else if (request.getParameter("button5") != null) {
					response.sendRedirect("ResetPassword.jsp");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			out.println(
					"<html><script>alert('Something went wrong'); window.location.href = 'UserHome.jsp';</script></html>");
		}

	}
}
