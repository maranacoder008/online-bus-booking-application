package com.bus.ServletClass;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class CancelTic
 */
@WebServlet("/CancelTic")
public class CancelTic extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CancelTic() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		try{
		Class.forName("com.mysql.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/BUS_DB", "root", "");
		HttpSession Session = request.getSession();
		if (Session.getAttribute("user") == null) {
			out.println("<html><script>alert('unAuthorized Access'); window.location.href = 'Login.jsp';</script></html>");
			//response.sendRedirect("Login.jsp");
		} else {
			int delet=(int)Integer.parseInt(request.getParameter("cancel"));
			PreparedStatement pt=connection.prepareStatement("delete from booking where bookid=?");
			pt.setInt(1, delet);
			//out.println("<html> <script> alert('are you sure!!!!'); <% '"+pt.execute()+"'><script><htnl>");
			PreparedStatement backup=connection.prepareStatement("insert into cancelled ( uid, busid, passname, sex, age, pkid, dpid, seatno, bookdate,deletedby) select  uid, busid, passname, sex, age, pkid, dpid, seatno, bookdate, 'UserCancelled' from booking where bookid=?");
			backup.setInt(1, delet);
			backup.execute();
			System.out.println("Record Deleted Successfully");
			pt.execute();
			Customer cust=(Customer)Session.getAttribute("customer");
			System.out.println(cust);
			out.println(cust.getHistory((int)Session.getAttribute("user")));
	}}catch(Exception e){
		e.printStackTrace();
		out.println("<html><script>alert('Something went Wrong!! Try Again'); window.location.href = 'UserHome.jsp';</script></html>");
	}
		}
}
