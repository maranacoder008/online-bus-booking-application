package com.bus.ServletClass;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class GetHisTicket
 */
@WebServlet("/GetHisTicket")
public class GetHisTicket extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetHisTicket() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession sess=request.getSession();
		if(sess.getAttribute("user")==null || sess.getAttribute("customer")==null){
			response.sendRedirect("Login.jsp");
		}
		else{
			try{
				Class.forName("com.mysql.jdbc.Driver");
				Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/BUS_DB", "root", "");
			if (request.getParameter("ticket") != null) {
				System.out.println(request.getParameter("ticket"));
				int ids=(int)Integer.parseInt(request.getParameter("ticket"));
				PreparedStatement ppt=connection.prepareStatement("select * from records where recordid=?");
				ppt.setInt(1, ids);
				ResultSet rss=ppt.executeQuery();
				rss.next();
				String[] arr=rss.getString(4).split(", ",7);
				TicketDetails dobj=new TicketDetails(arr.length);
				dobj.pickup=rss.getInt(6)+"";
				dobj.drop=rss.getInt(7)+"";
				
				Bus obj = new Bus(rss.getInt(3)	, (String)rss.getString(5));
				obj.getBusStruct();
				dobj.setObjects(arr, obj);
				sess.setAttribute("bobj", obj);
				sess.setAttribute("ticObj", dobj);
				sess.setAttribute("payment", true);
				response.sendRedirect("finalBill.jsp");
			} else if (request.getParameter("cancel") != null) {
				
				String str=request.getParameter("cancel");
				String[] arr=str.split(", ",7);
				for(int i=0;i<arr.length;i++){
				PreparedStatement pt=connection.prepareStatement("delete from booking where bookid=?");
				System.out.println(arr[i]);
				pt.setString(1, arr[i]);
				//out.println("<html> <script> alert('are you sure!!!!'); <% '"+pt.execute()+"'><script><htnl>");
				PreparedStatement backup=connection.prepareStatement("insert into cancelled ( uid, busid, passname, sex, age, pkid, dpid, seatno, bookdate,deletedby) select  uid, busid, passname, sex, age, pkid, dpid, seatno, bookdate, 'UserCancelled' from booking where bookid=?");
				backup.setString(1, arr[i]);
				backup.execute();
				System.out.println("Record Deleted Successfully");
				pt.execute();}
				PreparedStatement pt=connection.prepareStatement("delete from records where book_ids=?");
				pt.setString(1, str);
				pt.execute();
//				Customer cust=(Customer)sess.getAttribute("customer");
//				System.out.println(cust);
				response.sendRedirect("UserHome.jsp");
				
			}
			else{
				System.out.println("NOPE");
			}
			}catch(Exception e){
				e.printStackTrace();
				
				//response.sendRedirect("welcome.jsp");
			}
		}
		
	}

}
