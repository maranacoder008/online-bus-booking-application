package com.bus.ServletClass;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZonedDateTime;
import java.util.*;

public class Bus {
	Connection connection;
	private int busid;
	private String date;
	private String name;
	private int noofseats;
	private String phone;
	private int fare;
	private boolean[] arr;
	private ArrayList<String> pickup;
	private ArrayList<String> drop;
	private boolean[] vlist;
	Map<Integer, Boolean> lock=new HashMap<Integer, Boolean>();
	Bus(int id, String date) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/BUS_DB", "root", "");
			//lock.put(112, 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		busid = id;
		this.date = date;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getDate(){
		return this.date;
	}
	
	public int getBusid(){
		return this.busid;
	}
	public String getPhone(){
		return this.phone;
	}
	public boolean[] getVacantList(){
		return vlist;
	}
	public int getFare(){
		return fare;
	}
	public String viewBusStruct() throws Exception{
//		try {
			PreparedStatement stmt = connection.prepareStatement("select * from seat where busid=?");
			PreparedStatement statement = connection.prepareStatement("select * from bus where busid=?");
			stmt.setInt(1, busid);
			String ret = "";
			ResultSet resultset = stmt.executeQuery();
			ResultSet rs = resultset;
			statement.setInt(1, busid);
			ResultSet result = statement.executeQuery();
			result.next();
//			System.out.println(stmt);
//			System.out.println(statement);
			int r = 1;
			int sk=0;
			this.name=result.getString(2);
			this.noofseats=result.getInt(6);
			boolean[] ar=getVacant();
			this.phone=result.getString(7);
			
			ret = ret + "<html><link rel='stylesheet'href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'><head><style>table, th, td {border: 1px solid black;} .btn {"+
						"background-color: DodgerBlue; border: none; color: white; padding: 12px 16px; font-size: 16px; cursor: pointer;margin-top: 10px;margin-right: 5%;position:absolute;top:0;right:0;}"+
						"</style></head><center><br><div id='printMe' style='padding: 16px;width: 30%;'><h3>"+
					""+ result.getString("name") + "</h3></center><button class='btn' name='button3' value='button3'style='width: 60px;margin-top: 10px;margin-right: 95%;' onClick='moveback() ' ><i class='fa fa-arrow-left'>"+
						"</i> Back</button><script>function moveback(){window.history.back();}</script><form action='Logout' method='post'><button class='btn' name='button3' value='button3'style='width: 103px;margin-top: 10px;margin-right: 5%;'>"+
						"<i class='fa fa-trash'></i> Logout</button></form><center><div id='printMe' style='padding: 16px;width: 30%;'><table>";
			ret = ret + "<tr><td><center>w</td>";
			//System.out.print("W" + " ");
			int k=0;
			while (rs.next()) {
				k++;
				String ss = rs.getString("position");
				String[] str = ss.split("_", 3);
				int fare = rs.getInt("fare");
				String st = rs.getString("seattype");
				int a = Integer.parseInt(str[0]);
				if (a == 0) {
					//System.out.print(" || ");
					ret = ret + "<td width=100><center></td>";
				} else {
					if (a != r) {
						r = a;
						//System.out.println("W" + " ");
						ret = ret + "<td><center>w</td></tr>";
						//System.out.println();
						//System.out.print("W" + " ");
						ret = ret + "<tr><td><center>w</td>";
					}
				}
				lock=getLockList();
				System.out.println(lock);
				 if(lock.containsKey((Integer)rs.getInt("seatno"))){
					//System.out.println("waiting list");
					ar[Integer.parseInt(str[str.length - 1])-1]=true;
					ret = ret + "<td><center><table width=70 style=" + "'border:1px solid black'; background-color:#4db8ff" + "><tr><td style='background-color:#4db8ff'><center>";
					
					ret=ret	+ str[str.length - 1] + "</td>"/*<td><center>" + st + "</td>*/+"<td style='background-color:#4db8ff'><center>&#8377;" + fare
						+ "</td><tr></table></td>";
					sk++;
				}else if(ar[sk++]==false){
					ret = ret + "<td><center><table width=70 style=" + "'border:1px solid black'; background-color:#85e085" + "><tr><td style='background-color:#85e085'><center>";
					ret=ret	+ str[str.length - 1] + "</td>"/*<td><center>" + st + "</td>*/+"<td style='background-color:#85e085'><center>&#8377;" + fare
							+ "</td><tr></table></td>";
					}
				else{
					ret = ret + "<td><center><table width=70 style=" + "'border:1px solid black'; background-color:#ff6666" + "><tr><td style='background-color:#ff6666'><center>";
				
					ret=ret	+ str[str.length - 1] + "</td>"/*<td><center>" + st + "</td>*/+"<td style='background-color:#ff6666'><center>&#8377;" + fare
						+ "</td><tr></table></td>";}
				//System.out.print("	(" + str[str.length - 1] + "+" + st + "," + fare + ")	 ");
			}
			ret = ret + "<td><center>w</td>";
			//System.out.print("W");
			ret = ret + "</table></div></div></center>";
			vlist=ar;
			if(k==0){
				return "<html><script>alert('Sorry!!..No entries for this Bus in Database.'); window.location.href = 'welcome.jsp';</script></html>";
			}
			return ret;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return "";
	}

	public String showVacant() throws Exception{
//		try {

			PreparedStatement stmt = connection.prepareStatement("select count(*) from booking where bookdate=? and busid=?");
			stmt.setString(1, (date));
			stmt.setInt(2, busid);
//			PreparedStatement stmt1 = connection.prepareStatement("select noofseats from bus where busid=?");
//			stmt1.setInt(1, busid);
//			System.out.println(stmt);
			ResultSet rs = stmt.executeQuery();
			//ResultSet rss = stmt1.executeQuery();
			int r = 1;
			rs.next();
			//rss.next();
			int vacant = this.noofseats - rs.getInt(1);
			//System.out.println(rs.getInt(1));
			String op = "";
			op = "<lable><h3>Available seats: " + vacant + "</h3></lable></body></html>";
			if(vacant==0)
				op="<script>alert('Sorry! No Seats Available!!');window.history.back();</script> </body></html>";
			return op;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return "";
	}

	public boolean[] getVacant() throws Exception{
//		try {
			PreparedStatement stmt = connection.prepareStatement("select seatno from booking where booking.bookdate=? "
													+"and booking.busid=? order by seatno");
			stmt.setString(1, date);
			stmt.setInt(2, busid);
			ResultSet rs=stmt.executeQuery();
			 arr=new boolean[this.noofseats];
			while(rs.next()){
				int id=rs.getInt("seatno");
				arr[id-1]=true;
				//System.out.println(rs.getInt("seatno"));
			}	
			return arr;
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return new boolean[1];
	}
	
	public boolean removeBus()
	{
	try{
		if(busid==0)
			throw new Exception("Invalid Bus Selection");
		else{
			PreparedStatement stmt = connection.prepareStatement("delete from bus where busid=?");
			stmt.setInt(1, busid);
			return(stmt.execute());
		}
		
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return false;
		
	}
	
	public ArrayList getPickup() throws Exception{
//		try{
		PreparedStatement stmt= connection.prepareStatement("select pickpts,pickuptime from points where busid=?");
		stmt.setInt(1, this.busid);
		ResultSet result=stmt.executeQuery();
		int i=0;
		pickup= new ArrayList<String>();
		while(result.next()){
			pickup.add(i++, (result.getString(1)+" "+result.getString(2)));
		}
		return pickup;
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//		return pickup;
	}
	
	public ArrayList getDrop() throws Exception{
//		try{
		PreparedStatement stmt= connection.prepareStatement("select droppts,droptime from points where busid=?");
		stmt.setInt(1, this.busid);
		ResultSet result=stmt.executeQuery();
		int i=0;
		drop= new ArrayList<String>();
		while(result.next()){
			drop.add(i++, (result.getString(1)+" "+result.getString(2)));
		}
//		return drop;
//		}catch(Exception e){
//			e.printStackTrace();
//		}
		return drop;
	}
	
	public boolean bookTicket(TicketDetails obj, int user) throws Exception{
		if(obj==null || user<1){
			return false;
		}
			String res="";
			boolean r = false;
			PreparedStatement pt=connection.prepareStatement("select count(*) from booking where busid=? and bookdate=? and seatno=?");
			pt.setInt(1, this.busid);
			pt.setString(2, this.date);
			pt.setInt(3, obj.seatno[0]);
			try{
			ResultSet rsss=pt.executeQuery();
			rsss.next();
			if(rsss.getInt(1)>0)
				return false;
			}catch(Exception e){
			}
			int ppkid, dppid;
			pt=connection.prepareStatement("select points.pid from points where "
					+" points.busid="+this.busid+" and points.pickuptime='"+obj.pickup.substring(obj.pickup.length()-5,obj.pickup.length())+"'");
			//System.out.println(pt);
			ResultSet rs=pt.executeQuery();
			rs.next();
			ppkid=rs.getInt(1);
			
			PreparedStatement ptt=connection.prepareStatement("select pid from points where busid="+this.busid+" and droptime ='"+obj.drop.substring(obj.drop.length()-5,obj.drop.length())+"'");
			ResultSet rss=ptt.executeQuery();
			rss.next();
			dppid=rss.getInt(1);
			
			for(int i=0;i<obj.no;i++){
				long uid=ZonedDateTime.now().toInstant().toEpochMilli();
		PreparedStatement stmt=connection.prepareStatement("insert into booking(uid,busid,passname,sex,age,pkid,dpid,seatno,bookdate,bookid) values(?,?,?,?,?,?,?,?,?,?)");
		stmt.setInt(1, user);
		stmt.setInt(2, this.busid);
		stmt.setString(3, obj.passname[i]);
		if(obj.gender[i]==true)
		stmt.setString(4, "male");
		else
			stmt.setString(4, "female");
		stmt.setInt(5, obj.ages[i]);
		
		stmt.setInt(6, ppkid);
		stmt.setInt(7, dppid);
		stmt.setInt(8, obj.seatno[i]);
		stmt.setString(9, this.date);
		stmt.setLong(10, uid);
		r=stmt.execute();
		res+=uid+", ";
		PreparedStatement pr=connection.prepareStatement("delete from slock where date=? and busid=? and seatno =?"); 
		pr.setString(1, date);
		pr.setInt(2, this.busid);
		pr.setInt(3, obj.seatno[i]);
		lock.remove(obj.seatno[i]);
		System.out.println("Lock released: "+lock+" :"+pr.execute());
		
		
		}
			System.out.println(res);
			PreparedStatement pr=connection.prepareStatement("insert into records(uid, busid, book_ids, bookdate, pkid, dpid ) values (?,?,?,?,?,?)"); 
			pr.setInt(1, user);
			pr.setInt(2, this.busid);
			pr.setString(3, res.substring(0,res.length()-2));
			pr.setString(4, this.date);
			pr.setInt(5, ppkid);
			pr.setInt(6, dppid);
			pr.execute();
		return true;
		//return false;
	}
	
	public String getBusStruct() throws Exception{
		
		try {
			
			PreparedStatement statement = connection.prepareStatement("select * from bus where busid=?");
			
			String st="";
			statement.setInt(1, busid);
			ResultSet result = statement.executeQuery();
			result.next();
//			System.out.println(stmt);
//			System.out.println(statement);
			int r = 1;
			int sk=0;
			this.fare=result.getInt(12);
			this.name=result.getString(2);
			this.noofseats=result.getInt(6);
			boolean[] ar=getVacant();
			this.phone=result.getString(7);
			int fare=result.getInt("fare");
			lock=getLockList();
			st = st + "<html><link rel='stylesheet'href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'><head><style>table, th, td {border: 1px solid black; border-collapse:collapse;} .btn {"+
						"background-color: DodgerBlue; border: none; color: white; padding: 12px 16px; font-size: 16px; cursor: pointer;margin-top: 10px;margin-right: 5%;position:absolute;top:0;right:0;}"+
						"</style></head><center><br><div id='printMe' style='padding: 16px;width: 30%;'><h3>"+
					""+ result.getString("name") + "</h3></center><button class='btn' name='button3' value='button3'style='width: 60px;margin-top: 10px;margin-right: 95%;' onClick='moveback() ' ><i class='fa fa-arrow-left'>"+
						"</i> Back</button><script>function moveback(){window.history.back();}</script><form action='Logout' method='post'><button class='btn' name='button3' value='button3'style='width: 103px;margin-top: 10px;margin-right: 5%;'>"+
						"<i class='fa fa-trash'></i> Logout</button></form><center><div id='printMe' style='padding: 16px;width: 30%;'>";

		String input=result.getString("structure");
		int set=1;
		int left=Integer.parseInt(input.substring(0,1));
		int right=Integer.parseInt(input.substring(2,3));
		String ccode="#85e085";
		st+="<body><center><table>";
		while(set<=noofseats){
			if(lock.containsKey(set) )
				ccode="#4db8ff";
			else if(ar[(set-1)]==true)
				ccode="#ff6666";
			else
				 ccode="#85e085";
			st+="<tr style='width: 10%'>";
			for(int i=0;i<left;i++){
				if(lock.containsKey(set) )
					ccode="#4db8ff";
				else if(ar[(set-1)]==true)
					ccode="#ff6666";
				else
					 ccode="#85e085";
				st+="<td width= '80'><center><table title='Price:"+fare+"'><tr><td width= '75' style='background-color:"+ccode+"'><center>"+set+"</center></td></tr></table></center></td>";
				set++;
			}
			if(set>noofseats-(left+right)){
				try{
					
				if(input.substring(4,5).equals("1")){
					if(lock.containsKey(set))
						ccode="#4db8ff";
					else if(ar[(set-1)]==true)
						ccode="#ff6666";
					else
						 ccode="#85e085";
					st+="<td width= '80'><center><table title='Price:"+fare+"'><tr><td width= '75' style='background-color:"+ccode+"'><center>"+set+"</center></td></tr></table></center></td>";
					set++;
				}}
				catch(Exception e){
					st+="<td width= '60'></td>";
				}
			}else
			st+="<td width= 50></td>";
			for(int i=0;i<right;i++){
				if(lock.containsKey(set))
					ccode="#4db8ff";
				else if(ar[(set-1)]==true)
					ccode="#ff6666";
				else
					 ccode="#85e085";
				st+="<td width= '80'><center><table title='Price:"+fare+"'><tr><td width= '75' style='background-color:"+ccode+"'><center>"+set+"</center></td></tr></table></center></td>";
				set++;
			}
			st+="</tr>";
			
		}
		st+="</table></center></body></center></html>";
		
		return st;
	}catch(Exception e){
		e.printStackTrace();
		return "";
		
	}
		}
	
	HashMap getLockList() throws Exception{
		HashMap<Integer, Boolean> lmap=new HashMap<Integer, Boolean>();
		PreparedStatement stmt = connection.prepareStatement("select seatno from slock where date=? and busid=?");
		stmt.setString(1, date);
		stmt.setInt(2, busid);
		ResultSet rs=stmt.executeQuery();

		while(rs.next()){
			int id=rs.getInt("seatno");
			lmap.put(id, true);
//System.out.println(rs.getInt("seatno"));
}	
		return lmap;
	}
	
//	public static void main(String ar[]) throws Exception{
//		System.out.println(new Bus(1, "2018-12-12").getBusStruct(84, "3*3"));
//	}
	
}
