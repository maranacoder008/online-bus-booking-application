package com.bus.ServletClass;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class BookTicket
 */
@WebServlet("/BookTicket")
public class BookTicket extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BookTicket() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		PrintWriter out=response.getWriter();
		if (session.getAttribute("user") == null) {
			out.println("<html><script>alert('unAuthorized Access'); window.location.href = 'Login.jsp';</script></html>");
		} else {
			try{
			// Customer cobj=(Customer)session.getAttribute("");
			int no = Integer.parseInt(request.getParameter("nost"));
			TicketDetails oticdet = new TicketDetails(no);
			Bus bbobj=(Bus)session.getAttribute("bobj");
			oticdet.date=bbobj.getDate();
			for (int i = 0; i < no; i++) {
				oticdet.passname[i] = request.getParameter("pass" + (i + 1));
				oticdet.ages[i] = (int) Integer.parseInt(request.getParameter("age" + (i + 1)));
				oticdet.seatno[i] = (int) Integer.parseInt(request.getParameter("seat" + (i + 1)));
				if (request.getParameter("gen" + (i + 1)).equals("Male"))
					oticdet.gender[i] = true;
				else
					oticdet.gender[i] = false;
			}
			oticdet.pickup = request.getParameter("picks");
			oticdet.drop = request.getParameter("drops");
			// System.out.println(request.getParameter("pass"+i));
			// System.out.print(request.getParameter("age"+i));
			// System.out.print(request.getParameter("gen"+i));
			// System.out.println(request.getParameter("seat"+i));
			// for(int i=0;i<no;i++){
			// System.out.println(oticdet.passname[i]+" "+oticdet.ages[i]+"
			// "+oticdet.seatno[i]+" "+oticdet.pickup+" "+oticdet.drop);
			// }
			session.setAttribute("ticObj", oticdet);
			if(oticdet.checkDuplication(oticdet.seatno)==false){
				out.println("<html><lable style='color:#FF0000;'>Seat no's consist duplication!!! Try without repeating</lable></html>");
				RequestDispatcher req = request.getRequestDispatcher("ShowBus");
				req.include(request, response);
			}
			else
			response.sendRedirect("ShowBill.jsp");
			
		}catch(Exception e){
			out.println("<html><script>alert('Invalid Inputs!  Try Again'); window.location.href = 'welcome.jsp';</script></html>");
		}
		// System.out.println(request.getParameter("nost"));
	}}
}
