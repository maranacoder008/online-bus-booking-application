
package com.bus.ServletClass;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.*;


public class TicketDetails implements Serializable{
	TicketDetails(int k) {
		this.no = k;
		passname = new String[k];
		seatno = new int[k];
		ages = new int[k];
		gender = new boolean[k];
		fare = new int[k];
		total = 0;
		pay = 0;
		
	}

	int no;
	String[] passname;
	int[] seatno;
	int[] ages;
	boolean[] gender;
	String pickup;
	String drop;
	int fare[];
	int total;
	float pay;
	String date;
	//int busno;
	
	public int getno() {
		return this.no;
	}

	public String[] getpassname() {
		return passname;
	}

	public int[] getSeatno() {
		return seatno;
	}

	public int[] getAges() {
		return ages;
	}

	public boolean[] getGender() {
		return gender;
	}

	public void setPay(float p) {
		pay = p;
	}

	public float getPay() {
		return pay;
	}
	public int[] getfare(){
		return fare;
	}
	
	public String getPickup(){
		return this.pickup;
	}
	public String getDrop(){
		return this.drop;
	}
	public int[] getfare(int busid) {
		try {
			int amo=0;
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/BUS_DB", "root", "");
			PreparedStatement stmt = connection
					.prepareStatement("select fare from bus where busid=?");
			stmt.setInt(1, busid);
			ResultSet rs = stmt.executeQuery();
			rs.next();
			int ff=rs.getInt("fare");
			for (int i = 0; i < no; i++) {
				fare[i] = ff;
				amo += fare[i];
				//Bus.lock.put(busid+"_"+seatno[i], date);
				stmt=connection.prepareStatement("insert into slock(date,busid,seatno) values (?,?,?)");
				stmt.setString(1, date);
				stmt.setInt(2, busid);
				stmt.setInt(3, seatno[i]);
				
				System.out.println(" Inserted into lock:"+stmt.execute());
			}
			total=amo;
		}catch(SQLIntegrityConstraintViolationException ee){
			System.out.println("Do Not Refresh" + ee);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return fare;
	}
	
	public boolean checkDuplication(int[] arr){
		Map<Integer,Integer> map = new HashMap<>(); 
        int count = 0; 
        boolean dup = false; 
        for(int i = 0; i < no; i++){ 
            if(map.containsKey(arr[i])){ 
            		return false;
            } 
            else{ 
                map.put(arr[i], 1); 
            } 
        } 
		return true;
	}

	public int getTotal() {
		return this.total;
	}
	
	public void writeObjecttoDB(int id, String day) throws Exception{
		Class.forName("com.mysql.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/BUS_DB", "root", "");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream( baos );
       // TicketDetails obb=this;
        oos.writeObject( this );
        oos.close();
        PreparedStatement ps=connection.prepareStatement("insert into history(user,date,object) values(?,?,?)");
        ps.setInt(1, id);
        ps.setString(2, day);
        //System.out.println(baos);
        String st=Base64.getEncoder().encodeToString(baos.toByteArray());
        ps.setString(3, st);
        ps.execute();
        //System.out.println(st); 
	}
	
	public boolean setObjects(String[] arr, Bus bobj) throws Exception{
		Class.forName("com.mysql.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/BUS_DB", "root", "");
		String query="select * from booking where ";
		for(int i=0;i<no;i++){
			query+="bookid=?";
			if(i!=no-1)
				query+=" OR ";
		}
		query+=" order by bookid";
		System.out.println(query);
		PreparedStatement pt=connection.prepareStatement(query);
		for(int i=1;i<=no;i++){
			pt.setString(i, arr[i-1]);
		}
		System.out.println(pt);
		ResultSet rss=pt.executeQuery();
		//PreparedStatement stmt=connection.prepareStatement("select pickpts, pickuptime from points where ")
		for(int i=0;rss.next();i++){
			passname[i]=rss.getString(4);
			if(rss.getString(5).equals("male"))
				gender[i]=true;
			else
				gender[i]=false;
			ages[i]=rss.getInt(6);
			seatno[i]=rss.getInt(9);
			fare[i]=bobj.getFare();
			total+=fare[i];
		}
		pay=(total)+(float)(total/10);
		return false;
	}
	

}
