package com.bus.ServletClass;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Logout
 */
@WebServlet("/Logout")
public class Logout extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Logout() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		
		try{
		Class.forName("com.mysql.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/BUS_DB", "root", "");
		
		if(session.getAttribute("ticObj")!=null){
			TicketDetails obj=(TicketDetails)session.getAttribute("ticObj");
			Bus bobj=(Bus)session.getAttribute("bobj");
			if(bobj!=null && bobj.getDate()!=null)
			for(int i=0;i<obj.no;i++){
//				PreparedStatement pt=connection.prepareStatement("select seat.seatno from seat where seat.busid="+bobj.getBusid()
//						+" and seat.seatno="+obj.seatno[i]+"");
//				System.out.println(pt);
//				ResultSet rs=pt.executeQuery();
//				rs.next();
				PreparedStatement pr=connection.prepareStatement("delete from slock where date=? and busid=? and seatno =?"); 
				pr.setString(1, bobj.getDate());
				pr.setInt(2, bobj.getBusid());
				pr.setInt(3, obj.seatno[i]);
				//lock.remove(obj.seatno[i]);
				System.out.println("Lock released: "+pr.execute());
				//Bus.lock.remove(bobj.getBusid()+"_"+obj.seatno[i]);
			}
		}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		session.invalidate();
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP
																					// 1.1.
		response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
		response.setDateHeader("Expires", 0);
		//out.print("<html><script>var url = window.location.href;window.history.go(-window.history.length);window.location.href =  url;</script></html>");
		//request.getRequestDispatcher("/Login.jsp").include(request, response);
		response.sendRedirect("Login.jsp");
		//session.setAttribute("user", null);
		// Proxies.

		// chain.doFilter(req, res);
		//out.print("You are successfully logged out!");

		out.close();
	}

}
