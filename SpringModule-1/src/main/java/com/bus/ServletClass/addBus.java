package com.bus.ServletClass;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class addBus
 */
@WebServlet("/addBus")
public class addBus extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public addBus() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
//		System.out.println("Servlet Called");
//		
//		String name=(String)request.getParameter("name");
//		String phno=(String)request.getParameter("phone");
		int seatno=(int)Integer.parseInt(request.getParameter("noseat"));
//		String spec=(String)request.getParameter("spec");
//		String source=(String)request.getParameter("source");
//		String dest=(String)request.getParameter("dest");
//		System.out.println(name+" :"+ phno+" :"+seatno+" :"+spec+" :"+source+" :"+dest  );
//		System.out.println((String)request.getParameter("ptime"));
//		System.out.println((String)request.getParameter("dtime"));	
//		for(int i=1;i<=seatno;i++){
//			System.out.println(request.getParameter("spo"+i));
//			System.out.println(request.getParameter(("sfare"+i)));
//		}
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/BUS_DB", "root", "");
			PreparedStatement stmt = connection.prepareStatement("select MAX(busid) from bus");
			ResultSet re=stmt.executeQuery();
			re.next();
			int bid=re.getInt(1)+1;
			PreparedStatement prepare=connection.prepareStatement("insert into bus values(?,?,?,?,?,?,?,?,?,?)");
			prepare.setInt(1, bid);
			prepare.setString(2, (String)request.getParameter("name"));
			prepare.setString(3, (String)request.getParameter("source"));
			prepare.setString(4, (String)request.getParameter("dest"));
			prepare.setString(5, (String)request.getParameter("spec"));
			prepare.setInt(6, seatno);
			prepare.setString(7, (String)request.getParameter("phone"));
			prepare.setString(8, (String)request.getParameter("passw"));
			prepare.setString(9, (String)request.getParameter("ptime"));
			prepare.setString(10, (String)request.getParameter("dtime"));
			prepare.execute();
			
			int pp=Integer.parseInt((String)request.getParameter("nopk"));
			int dp=Integer.parseInt((String)request.getParameter("node"));
			
			
			int comm= (pp>dp)?dp:pp;
			System.out.println(pp+"::"+dp+"::"+comm);
			for(int i=1;i<=comm;i++){
				PreparedStatement st=connection.prepareStatement("insert into points(busid, pickpts, pickuptime, droppts, droptime) values(?,?,?,?,?)");
				st.setInt(1, bid);
				st.setString(2, (String)request.getParameter("sour"+i));
				st.setString(3, (String)request.getParameter("st"+i));
				st.setString(4, (String)request.getParameter("destpt"+i));
				st.setString(5, (String)request.getParameter("dt"+i));
				st.execute();
			}
			if(pp>comm)
			for(int i=1;i<=pp-comm;i++){
				PreparedStatement st=connection.prepareStatement("insert into points(busid, pickpts, pickuptime, droppts, droptime) values(?,?,?,NULL,NULL)");
				st.setInt(1, bid);
				st.setString(2, (String)request.getParameter("sour"+(i+comm)));
				st.setString(3, (String)request.getParameter("st"+(i+comm)));
				st.execute();
			}
			if(dp>comm)
			for(int i=1;i<=dp-comm;i++){
				PreparedStatement st=connection.prepareStatement("insert into points(busid,pickpts, pickuptime, droppts, droptime) values(?,NULL,NULL,?,?)");
				st.setInt(1, bid);
				st.setString(2, (String)request.getParameter("destpt"+(i+comm)));
				st.setString(3, (String)request.getParameter("dt"+(i+comm)));
				st.execute();
			}
			
			for(int i=1;i<=seatno;i++){
				PreparedStatement st=connection.prepareStatement("insert into seat(busid,seatno, seattype, position, fare) values(?,?,'Seater',?,?)");
				String temp=(String)request.getParameter("spo"+i);
				int t=getPosition(temp);
				if(t==0){
					out.print("<html><script>alert('Invalid Format of Data!! Try Again.'); window.location.href='addBus.jsp';</script></html>");
				}
				else{
					st.setInt(2, t);
				}
				st.setInt(1, bid);
				st.setString(3,"Seater");
				st.setString(4, temp);
				try{
				t=Integer.parseInt((String)request.getParameter("sfare"+i));
				st.setInt(5, t);
				}catch(Exception e){
					e.printStackTrace();
				}
				st.execute();
				}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	int getPosition(String st){

		String[] pat=st.split("_",4);
		if(pat.length<2 || pat.length>3){
			return 0;
		}
		for(int i=0;i<pat.length;i++){
			try{
				Integer.parseInt(pat[i]);
			}catch(Exception e){
				return 0;
			}
		}
		return Integer.parseInt(pat[pat.length-1]);
		
	}
	

}
