package com.bus.ServletClass;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.*;
/**
 * Servlet implementation class TicBooked
 */
@WebServlet("/TicBooked")
public class TicBooked extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TicBooked() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		try{
		HttpSession sess=request.getSession();
		if(sess.getAttribute("user")==null || sess.getAttribute("bobj")==null || sess.getAttribute("ticObj")==null ){
			out.println("<html><script>alert('unAuthorized Access'); window.location.href = 'Login.jsp';</script></html>");
			return;
		}
		Bus bobj=(Bus)sess.getAttribute("bobj");
		sess.setAttribute("payment", true);
		boolean result=bobj.bookTicket((TicketDetails)sess.getAttribute("ticObj"), (int)sess.getAttribute("user"));
			//((TicketDetails)sess.getAttribute("ticObj")).writeObjecttoDB((int)sess.getAttribute("user"),(String)sess.getAttribute("date"));
		//System.out.println(result);
		if(result==false){
			out.println("<html><script>alert('Something went wrong! Please try again')");
					response.sendRedirect("ShowBus");
		}
		else{
			out.println("<html><script> alert('Payment received Successfully!! Transaction completed!!');window.location.href='finalBill.jsp';</script></html>");
			//response.sendRedirect("finalBill.jsp");
			
		}
		}catch(Exception e){
			e.printStackTrace();
			out.println("<html><script>alert('Something went Wrong!! Try Again'); window.location.href = 'welcome.jsp';</script></html>");
		}
	}

}
