package com.bus.ServletClass;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Log
 */
@WebServlet("/Log")
public class Log extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Log() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		System.out.println("first login");
		RequestDispatcher rd = request.getRequestDispatcher("/Login.jsp");
		try{
		if (request.getParameter("uname") == null || request.getParameter("passw") == null) {
			out.println("<html><head><link rel='shortcut icon' type='image/x-icon' href='bus.png' /><title>Login</title></head><body color='red'><center><h4  color='red'><center>Wrong Constrains! Please try again!!</center></h4></center></body></html>");
			rd.include(request, response);
			
			return;
		}
		Customer obj = new Customer();
		if (obj.validateUser(request.getParameter("uname"), request.getParameter("passw"))) {
			System.out.println("Login Success");
			HttpSession Session = request.getSession();
			Session.setAttribute("user",obj.getid());
			Session.setAttribute("cobj", obj);
			System.out.println(obj.getName());
			Session.setAttribute("customer", obj);
			//out.println(new RetPassMail().Forgot(request.getParameter("uname")));
//			if(Session.getAttribute("ticObj")!=null){
//				RequestDispatcher rdd = request.getRequestDispatcher("ShowBill.jsp");
//				rdd.forward(request, response);
//			}
//			else{
			response.sendRedirect("UserHome.jsp");
			//}
		} else {
			out.println("<html><body><h4>wrong constrains! please try again</h4></body></html>");
			rd.include(request, response);
		}
	}catch(Exception e){
	e.printStackTrace();
	out.println(
			"<html><script>alert('Something went wrong'); window.location.href = 'opLogin.jsp';</script></html>");
	}
	}

}
