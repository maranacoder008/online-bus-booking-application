package com.bus.ServletClass;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

//$Id$

public class Operator{
	Connection connection;
	int busid;
	private ArrayList<Integer> seatno;
	private ArrayList<String> passname;
	private ArrayList<Boolean> gender;
	private ArrayList<Integer> age;
	private ArrayList<String> pkpoint;
	private ArrayList<String> droppoint;
	private ArrayList<Integer> fare;
	private ArrayList<String> phone;
	private String busname;
	private int total;
	
	Operator() throws Exception{
		try{
		Class.forName("com.mysql.jdbc.Driver");
		connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/BUS_DB", "root", "");
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("can't connect to the database");
			throw e;
		}
		total=0;
	}
	
	public int getBusid(){
		return this.busid;
	}
	
	public String getName(){
		return this.busname;
	}
	
	public ArrayList getSeat(){
		return this.seatno;
	}
	
	public ArrayList getPassname(){
		return this.passname;
	}
	
	public ArrayList getGender(){
		return this.gender;
	}
	
	public ArrayList getAge(){
		return this.age;
	}
	
	public ArrayList getPickpt(){
		return this.pkpoint;
	}
	
	public ArrayList getDroppt(){
		return this.droppoint;
	}
	
	public ArrayList getPhone(){
		return this.phone;
	}
	
	public ArrayList getFare(){
		return this.fare;
	}
	
	public int getTotal(){
		return this.total;
	}
	
	public boolean opLogin(String phno, String pass){
		
		try {
			PreparedStatement stmt = connection.prepareStatement("select busid,name from bus where ph_no=? and password=?");
			stmt.setString(1, phno);
			stmt.setString(2, pass);
			//System.out.println(stmt);
			ResultSet resultset = stmt.executeQuery();
			resultset.next();
			if (resultset != null) {
				this.busid=resultset.getInt(1);
				this.busname=resultset.getString(2);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean getTripDetails(String date) throws Exception {
		 total=0;
		 seatno=new ArrayList<Integer>();
		 passname=new ArrayList<String>();
		 gender=new ArrayList<Boolean>();
		 age=new ArrayList<Integer>();
		 pkpoint=new ArrayList<String>();
		 droppoint=new ArrayList<String>();
		 fare=new ArrayList<Integer>();
		 phone=new ArrayList<String>();
		try{
		PreparedStatement stmt = connection.prepareStatement("select booking.seatno, booking.passname, bus.fare, booking.sex,booking.age, user.phoneno, points.pickpts "+
								" from booking,user, points,bus where booking.bookdate=? and booking.busid=? and bus.busid=? and "+
								"booking.uid=user.uid and booking.pkid=points.pid order by booking.seatno");
		
		stmt.setString(1, date);
		stmt.setInt(2,this.busid);
		stmt.setInt(3, this.busid);
		//System.out.println(stmt);
		ResultSet result=stmt.executeQuery();
		
		PreparedStatement stemp = connection.prepareStatement("select droppts from points,booking where booking.bookdate=? and booking.busid=? and booking.dpid=points.pid");
		stemp.setString(1, date);
		stemp.setInt(2,this.busid);
		ResultSet temp=stemp.executeQuery();

		int c=0;
//		busname=result6.getString(1);
//		String st="<html><head><h2><center> Operator Statement</h2></head><body><h3>Bus Name:"+result6.getString(1)+"</h3><br><h4>Date:"+date+"</h4><br>";
//		st =st+ "<html><head><style>table, th, td {border: 1px solid black;} .button {line-height: 40px;width: 100px;font-size: 12pt;font-family: tahoma;"+
//				"margin-top: 3px;margin-right: 5px;background-color: green;position:absolute;top:0;right:0;}</style></head><body><br><h2>History</h2><br>"+
//				"<table style='background-color: green'><tr style='background-color: #32CD32'><td><center>Seat No</td><td><center> Passenger Name</td> "+
//				"<td><center> Male/Female</td> <td><center>Age</td><td>PickUp point</td><td><center>Drop Point</td><td><center>Mobile No</td><td><center>"+
//				"Fare</td></tr>";
		while(result.next() && temp.next()){
			c++;
//			st=st+"<tr style='background-color: white'><td><center>"+result.getInt(1)+"</td><td><center>"+
//					result.getString(2) +"</td><td><center>"+result.getString(4)+"</td><td><center>"+result.getInt(5)+
//					"</td><td><center>"+result.getString(7)+"</td><td><center>"+result.getString(8)+
//					"</td><td><center>"+result.getString(6)+"</td><td><center>"+result.getInt(3)+"</td></tr>";
			
			seatno.add(result.getInt(1));
			passname.add(result.getString(2));
			fare.add(result.getInt(3));
			if(((String)result.getString(4)).equals("male"))
				gender.add(true);
			else
				gender.add(false);
			
			age.add(result.getInt(5));
			phone.add(result.getString(6));
			pkpoint.add(result.getString(7));
			droppoint.add(temp.getString(1));
			total+=(int)result.getInt(3);
			
			
		}
		if(c==0)
			return false;
		else
			return true;
//		st=st+"</table>"
//				+ "<form action='Login.jsp'><Button class='button'>SIGN IN</Button></form></body></html>";
//		return st;
	}catch(Exception e){
		e.printStackTrace();
		throw e;
	}
		//return "<html><h2>NO CONTENT AVAILABE!!!!</h2></html>";
	}
	
	boolean cancelTrip(String date) throws Exception{
		
		try{
			PreparedStatement state=connection.prepareStatement("delete from booking where busid=? and bookdate=?");
			PreparedStatement backup=connection.prepareStatement("insert into cancelled ( uid, busid, passname, sex, age, pkid, dpid, seatno, bookdate, deletedby) select  uid, busid, passname, sex, age, pkid, dpid, seatno, bookdate, 'OperatorCancell' from booking where busid=? and bookdate=?");
			backup.setInt(1,this.busid);
			backup.setString(2, date);
			backup.execute();
			state.setInt(1, this.busid);
			state.setString(2, date);
			boolean result=state.execute();
			System.out.println("Canceling trip: "+result);
			return true;
		}catch(Exception e){
			e.printStackTrace();
			throw e;

		}
		
		
	}
	
//	public static void main(String ar[]){
//		Operator opobj=new Operator();
//		System.out.println(opobj.getTripDetails("2018-12-12", 1));
//	}
	
}
