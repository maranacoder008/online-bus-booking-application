package com.bus.ServletClass;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ChangePass
 */
public class ChangePass extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChangePass() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		try {
			HttpSession Session = request.getSession();
			if (Session.getAttribute("user") == null) {
				out.println(
						"<html><script>alert('unAuthorized Access'); window.location.href = 'Login.jsp';</script></html>");
				// response.sendRedirect("Login.jsp");
			}
			else if(Session.getAttribute("customer")==null){
				out.print("<html><script>alert('Session Expired!! Try Again'); window.location.href = 'Logout.jsp';</script></html>");
			}
			else {
				out = response.getWriter();
				String pas=(String)request.getParameter("passw2");
				if(!pas.equals((String)request.getParameter("passw3"))){
					out.print("<html><script>alert('New Password and Conform Password Must be Same.'); window.location.href = 'ResetPassword.jsp';</script></html>");
				}
				else if(pas.equals((String)request.getParameter("passw1"))){
					out.print("<html><script>alert('Your Current and New password are Same. Try with Differrent Passwords!'); window.location.href = 'ResetPassword.jsp';</script></html>");
				}
				else{
					Customer uobj=(Customer) Session.getAttribute("cobj");
					if(uobj.ChangePassword((String)request.getParameter("passw1"), pas)){
						out.print("<html><script>alert('Password Changed Sucessfully'); window.location.href = 'UserHome.jsp';</script></html>");
						
					}
					else{
						out.print("<html><script>alert('Your Current Password is not Correct'); window.location.href = 'ResetPassword.jsp';</script></html>");
						
					}
				}
			}}
		catch (Exception e) {
			e.printStackTrace();
			out.println(
					"<html><script>alert('Something went wrong'); window.location.href = 'UserHome.jsp';</script></html>");
		}
	}

}
