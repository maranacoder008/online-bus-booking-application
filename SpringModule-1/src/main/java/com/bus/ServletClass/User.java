package com.bus.ServletClass;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;

//$Id$

public class User {
	
	
	User() throws Exception {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			if(connection == null){
				connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/BUS_DB", "root", "");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Cannot connet to DataBase");
			throw e;
		}
	}

	private int uid;
	private String name;
	private String phno;
	private String email;
	Connection connection = null;

	public int getid() {
		return this.uid;
	}
	public void setid(int id) {
		uid = id;
	}
	public String getName() {
		return this.name;
	}

	boolean validateUser(String emailid, String passw) {
		try {
			PreparedStatement stmt = connection.prepareStatement("select * from user where email=? and password=?");
			stmt.setString(1, emailid);
			stmt.setString(2, passw);
			ResultSet resultset = stmt.executeQuery();
			resultset.next();
			if (resultset != null) {
				this.uid = resultset.getInt(1);
				this.phno = resultset.getString(3);
				this.email =emailid;
				this.name= resultset.getString(2);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	
	boolean ChangePassword(String oldp,String passw){
		
		if(uid<1){
			return false;
		}
		try{
			PreparedStatement stmt = connection.prepareStatement("select * from user where uid=? and password=?");
			stmt.setInt(1, uid);
			stmt.setString(2, oldp);
			ResultSet rss=stmt.executeQuery();
			if(rss.next()==false){
				if(rss.getString(1)==null)
					return false;
				return false;
			}
			stmt = connection.prepareStatement("update user set password=? where uid=?");
			stmt.setString(1, passw);
			stmt.setInt(2, uid);
			stmt.execute();
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
	}

	boolean addUser(String name, String email, String phno, String pass) {
		try {
			PreparedStatement stmt = connection
					.prepareStatement("insert into user (name,email,password,phoneno) values (?,?,?,?)");
			stmt.setString(1, name);
			stmt.setString(2, email);
			stmt.setString(3, pass);
			stmt.setString(4, phno);
			int r = stmt.executeUpdate();
			System.out.println("Inserted Sucessfully");
			this.name = name;
			this.phno = phno;
			this.email = email;
			stmt = connection.prepareStatement("select uid from user where email=?");
			stmt.setString(1, this.email);
			ResultSet rs = stmt.executeQuery();
			rs.next();
			this.uid = rs.getInt(1);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
