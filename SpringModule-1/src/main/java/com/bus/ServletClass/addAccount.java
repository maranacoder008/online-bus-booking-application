package com.bus.ServletClass;


import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class addAccount
 */
@WebServlet("/addAccount")
public class addAccount extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public addAccount() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			if(((String)request.getParameter("name"))==null || ((String)request.getParameter("phno"))==null || request.getParameter("uname")==null || request.getParameter("passw")==null){
				response.sendRedirect("NewRegister.jsp");
			}
			else{try{
				PrintWriter out=response.getWriter();
				Class.forName("com.mysql.jdbc.Driver");
				Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/BUS_DB", "root", "");
				PreparedStatement stmt = connection.prepareStatement("select count(*) from user where email=?");
				stmt.setString(1, request.getParameter("uname"));
				ResultSet result=stmt.executeQuery();
				result.next();
				if(result.getInt(1)!=0){
					out.println("<html><head><center><h3 style='color:red'> email already registered! click Forgot password</h3></center></head></html>");
					RequestDispatcher rd=request.getRequestDispatcher("/NewRegister.jsp");  
			        rd.include(request, response);  
				}
				else{
					Customer cobj=new Customer();
					boolean re=(cobj.addUser(request.getParameter("name"), request.getParameter("uname"), request.getParameter("phno"), request.getParameter("passw")));
					if(re==false){
						out.println("<html><head><center><h3 style='color:red'> Something went Wrong! Try Again !!</h3></center></head></html>");
						RequestDispatcher rd=request.getRequestDispatcher("/NewRegister.jsp");  
				        rd.include(request, response);
					}
					else{
						RequestDispatcher rd=request.getRequestDispatcher("Login.jsp");  
				        rd.forward(request, response);
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
	}
	}
}
