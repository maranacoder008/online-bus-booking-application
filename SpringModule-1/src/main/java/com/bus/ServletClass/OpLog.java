package com.bus.ServletClass;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class OpLog
 */
@WebServlet("/OpLog")
public class OpLog extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OpLog() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		PrintWriter out = response.getWriter();
		System.out.println("Operator login");
		RequestDispatcher rd = request.getRequestDispatcher("/opLogin.jsp");
		try{
		if (request.getParameter("mobno") == null || request.getParameter("passw") == null) {
			out.println("<html><body color='red'><center><h4>Wrong Constrains! Please try again!!</h4></body></html>");
			rd.include(request, response);
			return;
		}
		Operator opobj=new Operator();
		if (opobj.opLogin(request.getParameter("mobno"), request.getParameter("passw"))) {
			System.out.println("Operator Login Success");
			HttpSession Session = request.getSession();
			Session.setAttribute("opobj",opobj);
			//Session.setAttribute("date", request.getParameter("date"));
			System.out.println(opobj.getName());
//			if(!opobj.getTripDetails((String)Session.getAttribute("date"))){
//				Session.invalidate();
//			out.print("<html><script>alert('No Bookings Found'); window.location.href='opLogin.jsp';</script></html>");
//			}else	
			response.sendRedirect("OpHome.jsp");
			
		} else {
			out.println("<html><body><h4>wrong constrains! please try again</h4></body></html>");
			rd.include(request, response);
		}
	}catch(Exception e){
	e.printStackTrace();
	out.println(
			"<html><script>alert('Something went wrong'); window.location.href = 'opLogin.jsp';</script></html>");
	}
	}

}
