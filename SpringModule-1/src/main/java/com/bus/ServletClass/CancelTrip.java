package com.bus.ServletClass;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class CancelTrip
 */
@WebServlet("/CancelTrip")
public class CancelTrip extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CancelTrip() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();	
		HttpSession session=request.getSession();
		try{
			if(session.getAttribute("opobj")==null){
				out.print("<html><script>alert('This Opeartion can't be Processed!! Try Again'); window.location.href='Logout.jsp';</script></html>");
			}
			Operator opobj=(Operator)session.getAttribute("opobj");
			opobj.cancelTrip((String)session.getAttribute("date"));
			out.print("<html><script> alert('Trip Cancelled Successfully!!'); window.location.href='Logout.jsp';</script></html>");
		}catch(Exception e){
			e.printStackTrace();
			out.println("<html><script>alert('Something went wrong !!'); window.location.href='Logout.jsp';</script></html>");
		}
	}

}
