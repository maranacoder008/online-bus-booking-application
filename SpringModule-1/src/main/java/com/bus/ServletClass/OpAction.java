package com.bus.ServletClass;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class OpAction
 */
@WebServlet("/OpAction")
public class OpAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OpAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		try{
		
		HttpSession session=request.getSession();
		if(session.getAttribute("opobj")==null){
			out.print("<html><script>alert('Unauthorised Access!!'); window.location.href = 'opLogin.jsp';</script></html>");
		}
		else{
					//System.out.println((Operator)session.getAttribute("opobj"));
			if (request.getParameter("button1") != null) {
				session.setAttribute("date", request.getParameter("date"));
				if(!((Operator)session.getAttribute("opobj")).getTripDetails((String)session.getAttribute("date"))){
				out.print("<html><script>alert('No Bookings Found'); window.location.href='OpHome.jsp';</script></html>");
				}else	
				 response.sendRedirect("TripBill.jsp");
			} else if (request.getParameter("button2") != null) {
				System.out.println("Bus to delete");
				out.print("<html><script> var re=confirm('All of your datas will be lost!! Are you sure to delete'); if(re==true){document.location.href='TestClass';}else{window.location.href='OpHome.jsp';}</script></html>");
			} else if (request.getParameter("button3") != null) {
				RequestDispatcher rd = request.getRequestDispatcher("/Logout");
				rd.forward(request, response);
			}else if (request.getParameter("button4") != null) {
				if(!((Operator)session.getAttribute("opobj")).getTripDetails((String)session.getAttribute("date"))){
					out.print("<html><script>alert('No Bookings Found'); window.location.href='OpHome.jsp';</script></html>");
					}else
				response.sendRedirect("tripCancel.jsp");
			}
		}
		}catch(Exception e){
			e.printStackTrace();
			out.println(
					"<html><script>alert('Something went wrong'); window.location.href = 'opLogin.jsp';</script></html>");
			}
	}

}
