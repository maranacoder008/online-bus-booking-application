package com.bus.ServletClass;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.Date;

//import com.mysql.cj.protocol.Resultset;

//$Id$

public class Customer extends User {
			
	Customer() throws Exception  {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getHistory(int userid) {
		try {
			PreparedStatement stmt = connection.prepareStatement("select booking.bookdate, bus.name, seat.seatno,booking.passname, seat.fare, points.pickpts,points.pickuptime, points.droppts, points.droptime, booking.bookid from seat, booking, bus, points where booking.uid=? and"+
					" booking.pkid=points.pid and booking.busid=bus.busid and seat.seatid=booking.seatid order by booking.bookdate;");
			stmt.setInt(1, userid);
			ResultSet resultset = stmt.executeQuery();
//			if(!resultset.next()){
//				return "<html><head><h5>No Transactions Found in your Account</h5></head></html>";
//			}
			int k=0;
			String st = "<html><head> <link rel='shortcut icon' type='image/x-icon' href='bus.png' /><link rel='stylesheet'href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'><style>table, th, td {border: 1px solid black;} .button {line-height: 40px;width: 100px;"+
						"font-size: 12pt;font-family: tahoma;margin-top: 10px;margin-right: 5%;background-color: "+
						"orange;position:absolute;top:0;right:0;}  .button2 { line-height: 20px; width: 150px; font-size: 12pt;"+
						"font-family: tahoma; margin-top: 700px; margin-right: 45%; background-color: #32CD32; position: absolute;"+
						"top: 0; right: 0;}.btn {"+
						"background-color: DodgerBlue; border: none; color: white; padding: 12px 16px; font-size: 16px; cursor: pointer;margin-top: 10px;margin-right: 5%;position:absolute;top:0;right:0;}</style></head><body><br><h2>History</h2><br><table"+
						" style='background-color: green'><tr style='background-color: #32CD32'><td><center>Date of Travel"+
						"</td><td><center> Travel Bus</td> <td><center> Passenger Name</td> <td><center>Seat.No</td><td>FARE</td>"+
						"<td><center>Pickup Point</td><td><center>Pickup Time</td><td><center>Drop Point</td><td><center>Drop Time</td><td><center>Cancel Ticket</td></tr>";
			while(resultset.next()){
				k++;
				st=st+"<tr style='background-color: white'><form action='CancelTic' method='post'><td><center>"+resultset.getString(1)+"</td><td><center>"+resultset.getString(2) +
						"</td><td><center>"+resultset.getString(4)+"</td><td><center>"+resultset.getInt(3)+"</td><td><center>"+resultset.getInt(5)+
						"</td><td><center>"+resultset.getString(6)+"</td><td><center>"+resultset.getString(7)+"</td><td><center>"+resultset.getString(8)+
						"</td><td><center>"+resultset.getString(9)+"</td><td><center><button type='submit' name='cancel' value="+ resultset.getInt(10) + "  style='background-color:#ecc6d9;' onclick='myFunction()'> Cancel Ticket </button></form></tr>";
			}
			st=st+"</table><br><script>function myFunction() {alert('Cancellation charge will be Applied!'); location.reload();}</script><script>function funct() {window.location.href = 'UserHome.jsp';}</script><button type='button' class='button2' onClick=' funct() '><b>Back</b></button>"
					+ "<form action='Logout' method='post'><button class='btn' name='button3' value='button3'style='width: 103px;margin-top: 10px;margin-right: 5%;'><i class='fa fa-trash'></i> Logout</button></form></body></html>";
			//System.out.println(st);
			//getNewHistry();
			if(k==0){
				return 	"<html><script>alert('No Transactions Found !'); window.location.href = 'UserHome.jsp';</script></html>";

			}
			return st;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public String getCancelHistory(int userid) {
		try {
			PreparedStatement stmt = connection.prepareStatement("select cancelled.bookdate, bus.name, cancelled.seatno,cancelled.passname, bus.fare, points.pickpts,points.pickuptime,"+
					" points.droppts, points.droptime,cancelled.deletedby from cancelled, bus, points where cancelled.uid=? and"+
					" cancelled.pkid=points.pid and cancelled.busid=bus.busid order by cancelled.bookdate;");
			stmt.setInt(1, userid);
			ResultSet resultset = stmt.executeQuery();
//			if(!resultset.next()){
//				return "<html><head><h5>No Transactions Found in your Account</h5></head></html>";
//			}
			int k=0;
			String st = "<html><head><link rel='shortcut icon' type='image/x-icon' href='bus.png' /><link rel='stylesheet'href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'><style>table, th, td {border: 1px solid black;border-collapse: collapse;} .button {line-height: 40px;width: 100px;"+
						"font-size: 12pt;font-family: tahoma;margin-top: 10px;margin-right: 5%;background-color: "+
						"orange;position:absolute;top:0;right:0;}  .button2 { line-height: 20px; width: 150px; font-size: 12pt;"+
						"font-family: tahoma; margin-top: 700px; margin-right: 45%; background-color: #32CD32; position: absolute;"+
						"top: 0; right: 0;}.btn {"+
						"background-color: DodgerBlue; border: none; color: white; padding: 12px 16px; font-size: 16px; cursor: pointer;"+
						"margin-top: 10px;margin-right: 5%;position:absolute;top:0;right:0;}</style></head><body><br><h2> Cancelled Ticket History</h2><br><table"+
						" style='background-color: green'><tr style='background-color: #32CD32'><td><center>Date of Travel"+
						"</td><td><center> Travel Bus</td> <td><center> Passenger Name</td> <td><center>Seat.No</td><td>FARE</td>"+
						"<td><center>Pickup Point</td><td><center>Pickup Time</td><td><center>Drop Point</td><td><center>Drop Time</td><td><center>Cancellation Reason</td><td><center>Refund status</td></tr>";
			while(resultset.next()){
				k++;
				st=st+"<tr style='background-color: white'><td><center>"+resultset.getString(1)+"</td><td><center>"+resultset.getString(2) +
						"</td><td><center>"+resultset.getString(4)+"</td><td><center>"+resultset.getInt(3)+"</td><td><center>"+resultset.getInt(5)+
						"</td><td><center>"+resultset.getString(6).substring(0,1).toUpperCase()+resultset.getString(6).substring(1)+"</td><td><center>"+resultset.getString(7)+"</td><td><center>"+resultset.getString(8).substring(0,1).toUpperCase()+resultset.getString(8).substring(1)+
						"</td><td><center>"+resultset.getString(9)+"</td><td><center>"+ resultset.getString(10) + " </td><td><center>Refund Initialized</td></tr>";
			}
			st=st+"</table><br><script>function funct() {window.location.href = 'UserHome.jsp';}</script><button type='button' class='button2' onClick=' funct() '><b>Back</b></button>"
					+ "<form action='Logout' method='post'><button class='btn' name='button3' value='button3'style='width: 103px;margin-top: 10px;margin-right: 5%;'><i class='fa fa-trash'></i> Logout</button></form></body></html>";
			//System.out.println(st);
			//getNewHistry();
			if(k==0){
				return 	"<html><script>alert('No Transactions Found !'); window.location.href = 'UserHome.jsp';</script></html>";

			}
			return st;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public TicketDetails getNewHistry() throws Exception{
		PreparedStatement stmt = connection.prepareStatement("select * from history where user=?");
		//System.out.println(super.getid());
		stmt.setInt(1, super.getid());
		System.out.println(stmt);
		ResultSet rs=stmt.executeQuery();
		//rs.next();
		TicketDetails o=null;
		while(rs.next()){
		String s=rs.getString(4);
		 byte [] data = Base64.getDecoder().decode( s );
	        ObjectInputStream ois = new ObjectInputStream( new ByteArrayInputStream(data));
	        o  = (TicketDetails)ois.readObject();
	        for(int i=0;i<o.passname.length;i++){
	        System.out.println(o.passname[0]);
	        System.out.print(" : "+o.getDrop());
	        System.out.print(" : "+o.getPickup());
	        }
	        ois.close();
		}
		
		return o;
	}
	
	public String getOverHistory(int userid) throws Exception{
		String result="";
		int count=0;
		try{
		PreparedStatement stmt=connection.prepareStatement("select * from records where uid=? order by bookdate	");
		stmt.setInt(1, userid);
		ResultSet rs=stmt.executeQuery();
		
		String color;
		int i=1;
		 result="<html><link rel='stylesheet'href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'><head><link rel='shortcut icon' type='image/x-icon' href='bus.png' /></head><style>table, th, td {border: 1px solid black; border-collapse: collapse;}.btn {"+
						"background-color: DodgerBlue; border: none; color: white; padding: 12px 16px; font-size: 16px; cursor: pointer;margin-top: 10px;margin-right: 5%;position:absolute;top:0;right:0;}</style><body><br><h3><b>Booking History:</b></h3><br><table>"+
						"<tr><td><center>S.No</center></td><td><center>Bus Booked</center></td><td><center>Book Date</center></td><td><center>Source City</center></td><td><center>Destination City</center></td><td><center>View Ticket</center></td>"+
						" <td>Cancel Ticket</td> <td>Status</td></tr>";
		while(rs.next()){
			System.out.println(rs.getString(5));
			String gdate=rs.getString(5);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date dat=sdf.parse(gdate);
			Date date=new Date();
				System.out.println(date);
				PreparedStatement pre=connection.prepareStatement("select name,source,destination from bus where busid=?");
				pre.setInt(1, rs.getInt(3));
				ResultSet ss=pre.executeQuery();
				ss.next();
				if(dat.before(date))
					result+="<form action='GetHisTicket' method='post'><tr style='background-color:"+"#ffa366"+"'><td><center>"+(i++)+"</center></td><td width=170px><center>"+ss.getString(1).substring(0, 1).toUpperCase()+ss.getString(1).substring(1) +"</center></td><td width=120px><center>"+rs.getString(5) +"</center></td><td width=120px><center>"+ss.getString(2).substring(0, 1).toUpperCase()+ss.getString(2).substring(1)  +"</center></td><td width=20%><center>"+ss.getString(3).substring(0, 1).toUpperCase()+ss.getString(3).substring(1) +"</center></td><td width=10%><center><button type='submit' name='ticket' value='"+rs.getInt(1)+"'>Ticket</button></center> </td><td></td><td width=10%> Completed</td></tr></form>";
				else{
					long difference = (dat.getTime() - date.getTime())/86400000;
				      
					result+="<form action='GetHisTicket' method='post'><tr style='background-color:"+"#33ffad"+"'><td><center>"+(i++)+"</center></td><td width=170px><center>"+ss.getString(1).substring(0, 1).toUpperCase()+ss.getString(1).substring(1) +"</center></td><td width=120px><center>"+rs.getString(5) +"</center></td><td width=120px><center>"+ss.getString(2).substring(0, 1).toUpperCase()+ss.getString(2).substring(1)  +"</center></td><td width=20%><center>"+ss.getString(3).substring(0, 1).toUpperCase()+ss.getString(3).substring(1) +"</center></td><td width=10%><center><button type='submit' name='ticket' value='"+rs.getInt(1)+"'>Ticket</button></center> </td><td width=10%><center><button type='submit' name='cancel' value='"+rs.getString(4)+"'>Cancel</button></center> </td><td width=15%><center>"+difference+" Days to Go</center></td></tr></form>";
				}
				count++;
		}
		result+="</table><script>function funct() {window.location.href = 'UserHome.jsp';}</script><button type='button' class='button2' onClick=' funct() '><b>Back</b></button><form action='Logout' method='post'><button class='btn' name='button3' value='button3'style='width: 103px;margin-top: 10px;margin-right: 5%;'><i class='fa fa-trash'></i> Logout</button></form></body></html>";
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		//System.out.println(result);
		if(count==0)
			return "<html><script>alert('No Records Found!!'); window.location.href='UserHome.jsp'; </script></html>";
		return result;
		
	}
	
	
//	public static void main(String ar[]){
//		Customer cobj=new Customer();
//		cobj.getHistory(1);
//	}
}
