//$Id$
package com.bus.ServletClass;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.Properties;
import java.util.Random;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.sun.mail.smtp.SMTPTransport;

public class RetPassMail {

	public static String Forgot(String to) {

        final String username = "vikky.any";
        final String password = "Vikky.any98";

        StringBuilder finalEmail = new StringBuilder();
       finalEmail.append("<html><body><table border='1' cellpadding='1' cellspacing='1' width='50%' style='max-width: 600px;'><center><h1 style='margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;'>Reset Your Password</h1><br><p style='margin: 0;'>See below to reset your customer account password. If you didn't request a new password, you can safely delete this email.</p>");
       finalEmail.append("<br><h3   target='_blank' style='display: inline-block; padding: 16px 36px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; color: #ffffff; text-decoration: none; border-radius: 6px;'>");
       
       try{
       		Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/BUS_DB", "root", "");
			PreparedStatement stmt = connection.prepareStatement("select password from user where email=?");
			stmt.setString(1, to);
			ResultSet rs=stmt.executeQuery();
			if(rs.next()){
				
				String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890asdfghjklzxcvbnmqwertyuiop";
		        StringBuilder salt = new StringBuilder();
		        Random rnd = new Random();
		        while (salt.length() < 10) { // length of the random string.
		            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
		            salt.append(SALTCHARS.charAt(index));
		        }
		        String saltStr = salt.toString();
		         stmt = connection.prepareStatement("update user set password=? where email=?");
		        stmt.setString(1, saltStr);
				stmt.setString(2, to);
				
				finalEmail.append(saltStr+"</h3><br><tr align='left' bgcolor='#ffffff' style='padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;'><p style='margin: 0;'>Kindly Go to the Link, copy and paste the following link in your browser:</p></tr><tr><p style='margin: 0;'><a href='http://172.22.104.239:8080/SpringModule-1/Login.jsp' target='_blank'> click here to login</a></p></tr><p style='margin: 0;'><br><br><br>Yours,<br> Vikky</p></center></table>");
				finalEmail.append("<center><table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 600px;'> <tr><td align='center' bgcolor='#e9ecef' style='padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;'><p style='margin: 0;'>You received this email because we received a request for bus application for your account. If you didn't request [type_of_action] you can safely delete this email.</p></td></tr><tr><td align='center' bgcolor='#e9ecef' style='padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;'><p style='margin: 0;'>To stop receiving these emails, you can <a href='' target='_blank'>unsubscribe</a> at any time.</p><p style='margin: 0;'>Paste 1234 S. Broadway St. City, State 12345</p></td></tr></table></td></tr></table></body></html>");		
				//System.out.println(finalEmail.toString());
			            Send(username, password, to, "", "Forgot Password ", finalEmail.toString());
			            stmt.execute();
			            System.out.println("Mail Sent");
			            return "<html><script>alert('Password sent to your email'); window.location.href = 'Login.jsp';</script></html>";
			        
			}
			else{
				return "<html><script>alert('This E-Mail is not Registered!! '); window.location.href = 'ForgotPass.jsp';</script></html>";
			}
			
       	}catch(Exception e){
       		e.printStackTrace();
       		System.out.println(e);
       		return "<html><script>alert('Something went wrong!!'); window.location.href = 'Login.jsp';</script></html>";
       	}
        // message.setContent(finalEmail.toString(), "text/html");
       

    }

    public static void Send(final String username, final String password, String recipientEmail, String ccEmail, String title, String message) throws AddressException, MessagingException {
        //  Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

        final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";

        // Get a Properties object
        Properties props = System.getProperties();
        props.setProperty("mail.smtps.host", "smtp.zoho.com");
        props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
        props.setProperty("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.port", "465");
        props.setProperty("mail.smtp.socketFactory.port", "465");
        props.setProperty("mail.smtps.auth", "true");

        /*
         If set to false, the QUIT command is sent and the connection is immediately closed. If set 
         to true (the default), causes the transport to wait for the response to the QUIT command.

         ref :   http://java.sun.com/products/javamail/javadocs/com/sun/mail/smtp/package-summary.html
                 http://forum.java.sun.com/thread.jspa?threadID=5205249
                 smtpsend.java - demo program from javamail
         */
        props.put("mail.smtps.quitwait", "false");

        Session session = Session.getInstance(props, null);

        // -- Create a new message --
        final MimeMessage msg = new MimeMessage(session);

        // -- Set the FROM and TO fields --
        msg.setFrom(new InternetAddress(username + "@zoho.com"));
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientEmail, false));

        if (ccEmail.length() > 0) {
            msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail, false));
        }

        msg.setSubject(title);
        msg.setContent(message, "text/html");
        msg.setSentDate(new Date());

        SMTPTransport t = (SMTPTransport) session.getTransport("smtps");

        t.connect("smtp.zoho.com", username, password);
        t.sendMessage(msg, msg.getAllRecipients());
        t.close();
    }
		}