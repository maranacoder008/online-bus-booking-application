package com.bus.ServletClass;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.*;

/**
 * Servlet implementation class ShowBus
 */
@WebServlet("/ShowBus")
public class ShowBus extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ShowBus() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//HttpSession Session = request.getSession();
		PrintWriter out = response.getWriter();
		int busid;
		try{
			HttpSession Session = request.getSession();
			
		try {
			
			if (Session.getAttribute("user") != null) {
				try{
					 busid = Integer.valueOf(request.getParameter("bus"));
					 Session.setAttribute("busid",request.getParameter("bus") );
				}catch(Exception e){
					 busid=Integer.valueOf( (String)Session.getAttribute("busid"));
				}
				System.out.println(Session.getAttribute("date") + " : " + Session.getAttribute("source") + " : "
						+ Session.getAttribute("dest"));
				Class.forName("com.mysql.jdbc.Driver");
				Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/BUS_DB", "root", "");
				PreparedStatement stmt = connection.prepareStatement("select * from bus where busid=?");
				stmt.setInt(1, busid);
//				System.out.println(stmt);
				ResultSet resultset = stmt.executeQuery();
				ResultSet rs = resultset;
				rs.next();
				// out.println(rs.getString("name"));
				
				Bus obj = new Bus(busid, (String)Session.getAttribute("date"));
					obj.viewBusStruct();
				if(obj.getDate()==null){
					response.sendRedirect("welcome.jsp"); 
				}
				else
				out.print((obj.getBusStruct() + obj.showVacant()));
			
				// obj.showVacant("2018-12-12", 1);
				//HttpSession session=request.getSession();
				Session.setAttribute("bobj", obj);
				RequestDispatcher rd = request.getRequestDispatcher("/bookList.jsp");
				rd.include(request, response);
				
			} else {
				RequestDispatcher rd = request.getRequestDispatcher("/SecLog.jsp");
				Session.setAttribute("busid",request.getParameter("bus") );
				rd.forward(request, response);
			}
		
		} catch (Exception e) {
			e.printStackTrace();
			out.println("<html><script>alert('Something Went Wrong!  Try Again'); window.location.href = 'welcome.jsp';</script></html>");
		}
		}catch(Exception e){
			e.printStackTrace();
			out.println("<html><script>alert('Invalid Inputs!  Try Again'); window.location.href = 'welcome.jsp';</script></html>");

		}
	}
}
