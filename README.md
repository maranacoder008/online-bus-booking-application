# Online Bus Booking Application

Application developed by Vignesh.P


		1. Clone the Source Code using -git clone https://maranacoder008@bitbucket.org/maranacoder008/online-bus-booking-application.git
		2. Open Eclipse IDE and Make a new Servlet Project named 'New Bus'.
		3. Copy the WebContent, src and paste those in created project.
		4. Go to the WEB.xml file and make the welcome page name as 'Login.jsp'.
		5. Download the 'mysql-connector-java-8.0.12.jar'.
		6. Include the JAR file inside 'New Bus/Java Resources/Referenced Libraries'.
		7. Make sure that current Server as 'Tomcat v8.0 Server at Localhost' is working Properly.
		8. Open console and connect to MYSQL.
		9. Set a new mysql named 'root' and password ''.
		10. Stop and start the MySQL service. 
		11. Create a Database named 'BUS_DB'.
		12. Then Execute the Queries with Respective tables in 'Bus_Application_DB_Query.doc' file.
		13. Now Start the Server and Execute the Project in your Server.
		
		